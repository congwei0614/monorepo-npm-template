# iv-npm

[TOC]

## 介绍

使用pnpm+workspace+turborepo实现的monorepo管理方式的公共npm包
项目使用pnpm作为包管理工具。请自行全局下载。

## 环境介绍

- "node": "^18.18.0 || >=21.1.0"

## 注意事项

- 对于打包、发布、修订版本号这些操作，直接使用`npm run push`完成即可
- **对于每次代码改动提交，请务必及时更新文档，保证文档信息一致性**
- 代码的改动请保证向下兼容，不要影响现有功能。

## 安装依赖

> pnpm install

## 打包发布

在登陆过npm情况下，发布需要修改**根下version**，并关闭所有的private属性。

> pnpm run push

## 使用

> npm i iv-npm

```ts
// main.ts
import ivUi from "iv-npm/packages/ui";
app.use(ivUi);
```

```vue
// UI组件使用方法
<template>
  <IVMrpModal v-model:show="Visible"></IVMrpModal>
</template>
<script setup lang="ts">
// 内部有自动注册，可以不用显示引入
import { IVMrpModal } from "iv-npm/packages/ui";
</script>
```

```vue
// 工具函数使用方法
<script setup lang="ts">
import { findTree } from "iv-npm/packages/shared/utils";
</script>
```

## 公用函数文档

### 数组

数组方法全部使用`number-precision`插件进行计算，从而避免出现js小数计算精度问题。同时也将会导出` strip, plus, minus, times, divide, round`这些由插件提供方法，具体细节请常查看[**number-precision**文档](https://github.com/nefe/number-precision)。

**formatPercentage**

计算两个数字的百分百比

```typescript
// complete 分子
// need 分母
export const formatPercentage: (complete: number, need: number) => number;
```

**arraySum**

计算数组之和

```typescript
// arr 计算数组
const arraySum: (arr: number[]) => number;
```

**arraySumByKey**

计算数组对象中某个字段之和

```typescript
// arr 计算数组对象
// key 数组对象某key值
const arraySumByKey: (arr: any[], key: string) => number;
```

**isArraySumCompared**

判断两个数组对象中某个字段之和是否相同

```typescript
// arr1 数组对象1
// arr2 数组对象2
// key1 数组对象1中某个key值
// key2 数组对象2中某个key值，如果未传递则使用key1
const isArraySumCompared: (
  arr1: any[],
  arr2: any[],
  key1: string,
  key2?: string,
) => boolean;
```

### 树结构

**arrToTree**

数组转树结构（两级树结构），返回树结构中子级字段为children

逻辑简介

> - 遍历数据
> - 如果当前元素不存在父级标记，则表示当前元素为父级元素
>   - 找到数据中所有父级标记与当前元素的子级标记相同的的数据
>   - 如果找到的数据不为空，则为当前元素添加子级元素列表children

```typescript
// list 需要转换的数组
// parentMark 父级标记，子级中指向当前元素父级的字段
// childrenMark 子级标记，父级中指向当前元素子级的字段
const arrToTree: (list: any[], parentMark: string, childrenMark: string) => any;
```

**tranListToTreeData**

数组转树结构（多级树结构），返回树结构中子级字段为children

逻辑简介

> - 遍历数据
> - 在数据中过滤出parentId为null的元素，表示第一层数据。
> - 用第一层数据去递归加载自己的子项（1）
>   - 找到数据中所有id于当前元素的parentId相等的数据
>   - 将其添加到当前元素的子项children
>   - 再次遍历当前元素子项去递归（1）当前元素子项的子项

```typescript
// list 需要转换的数组
interface TreeList {
  parentId: string | null; // 父级标记，子级中指向当前元素父级的字段
  id: string; // 子级标记，父级中指向当前元素子级的字段
  children?: TreeList[]; // 返回子级数据
}
const tranListToTreeData<T extends TreeList>: (list: T[]) => T[]
```

**findTree**

通过递归方式查询树结构中某一个元素

```typescript
// id 需要查询元素的value值
// list 需要查询的数据
// idName 需要查询元素的key值
const findTree<T extends {
    id: string;
    children?: T[] | undefined;
}>: (id: string, list: T[], idName?: any) => any
```

### 金额格式化

**numFormat**

增加金额千分位标记和保留某位小数

```typescript
// num 需要格式化金额
// suffix 保留小数，默认保留2位
const numFormat: (num: number, suffix?: number) => string;
```

**getAmountByUnit**

将金额转换为大写，最大九位数。不足用¥填充，用于金额模板打印

```typescript
// amount 金额
const getAmountByUnit: (amount: number) => string;
```

~~**amountToFixed（废弃，numFormat替代）**~~

将金额保留两位小数。如果包含元字，处理结果也会返回元

```typescript
// _v 金额
const amountToFixed: (_v?: number | string) => string | 0;
```

~~**amountToThousandthFixed（废弃，numFormat替代）**~~

将金额加千分符并保留两位小数。如果包含元字，处理结果也会返回元

```typescript
// num 金额
// fixed 保留小数位
const amountToThousandthFixed: (
  num?: number | string,
  fixed?: number,
) => string | 0;
```

### ~~日期（废弃）~~

日期处理函数建议使用DayJs替代。

```typescript
type formatType = {
  date: string;
  time: string;
  week: string;
};
```

**dateFormat**

格式化日期

- 未传递参数则使用当前时间
- 传递时间未传递格式化模板则返回FormatType对象
- 传递时间且传递格式化模板则返回格式化时间字符串

```typescript
// data 时间
// format 格式化模板（YYYY-MM-DD hh:mm:ss）
const dateFormat: () => formatType;
const dateFormat: (data: Date | string, format: string) => string;
const dateFormat: (
  data?: Date | string,
  format?: string,
) => string | formatType;
```

**yearMonthFormat**

格式化日期，如果传递日期则返回YYYY年MM月，否则返回空串

```typescript
const yearMonthFormat: (data?: Date) => string | formatType;
```

**yearMonthDayFormat**

格式化日期，如果传递日期则返回YYYY年MM月DD日，否则返回空串

```typescript
const yearMonthDayFormat: (data?: Date) => string | formatType;
```

### 枚举

**ProjectStausEnum**

```typescript
// 项目状态枚举
enum ProjectStausEnum {
  "待开工" = 0,
  "在建" = 1,
  "完工未结算" = 2,
  "完工已结算" = 4,
  "已销项" = 8,
}
```

**SignStatusEnum**

```typescript
// 签证状态枚举
enum SignStatusEnum {
  "待签约" = 1,
  "已签约" = 2,
  "已完结" = 4,
  "诉讼中" = 8,
}
```

**LineIDEnum**

```typescript
// 条线ID枚举
enum LineIDEnum {
  "tx01" = 1, // 合约
  "tx02" = 1 << 1, // 材料
  "tx03" = 1 << 2, // 资产
  "tx04" = 1 << 3, // 市场
  "tx05" = 1 << 4, // 技术,
}
```

### API请求处理函数

**buildURL**

构建get请求url，将参数添加到地址中。（**内部ui组件使用**）

```typescript
// url
// params get形式参数对象
const buildURL: (url: string, params: any) => string;
```

**hostConfiger**

构建请求Host地址。区分本地、nacho以及stecip环境。（**内部ui组件使用**）

```typescript
type HostKey =
  | "BASE_URL"
  | "MRP_URL"
  | "MRP2_URL"
  | "ASSET_URL"
  | "ASSETS_URL"
  | "GATEWAY_URL"
  | "SFM_URL"
  | "CCFLOW_URL"
  | "SHARECOOL_URL"
  | "PDM_URL";
const hostConfiger: (key: HostKey) => string;
```

**apiConfiger**

对传入配置修改baseUrl属性，通常传入配置为Axios拦截器config，同时区分本地、nacho以及stecip环境

可以理解为函数会在不同环境返回相应的baseUrl，以实现环境变量控制接口。

同时需要注意在使用时需要给congfig传递requestBase来区分多个域名。具体值自行查看。

- 本地开发config.baseUrl返回值为vite.config.server.proxy的代理key值，以实现本地代理。
- nacho环境config.baseUrl返回值为根据requestBase对应的www.XX.nacho.cn的域名。
- stecip环境config.baseUrl返回值为根据requestBase对应的www.XX.stecip.com的域名

```typescript
const apiConfiger: (config: any) => any;
```

### 工具函数

**isPlainObject**

检查对象是否为普通对象（使用“{}”或“new Object”创建）

```typescript
const isPlainObject: (obj: object) => boolean;
```

**isObject**

判断元素是否是一个对象

```typescript
const isObject: (val: unknown) => val is Record<any, any>;
```

**isDate**

判断元素是否是一个日期对象

```typescript
const isDate: (val: any) => boolean;
```

**isURLSearchParams**

是否是一个URLSearchParams

```typescript
const isURLSearchParams: (val: any) => boolean;
```

**filterOptionHeadle**

Antv下拉框搜索处理函数

```typescript
// key 搜索过滤的key值，需要在下拉框子项元素绑定key的数据
const filterOptionHeadle: (key: string) => (input: string, option: any) => any;
```

## ui组件文档

### 树形下拉框选择

#### IVAccessoryTypeSelector

**说明**

耗材配件类型，树形下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                                                   | 类型    | 默认值                                                      |
| -------------- | ---------------------------------------------------------------------- | ------- | ----------------------------------------------------------- |
| value(v-model) | 指定当前选中的条目                                                     | any     |                                                             |
| params         | 接口请求参数（get方式）                                                | object  | {}                                                          |
| data           | 接口请求参数（post方式）                                               | object  | {}                                                          |
| replaceFields  | 替换 treeNode 中 label,value,key,children 字段为 treeData 中对应的字段 | object  | {children:'children', label:'name', key:'id', value: 'id' } |
| inputStyle     | 树选择框的style样式                                                    | object  | {}                                                          |
| formItemStyle  | AFormItem的style样式                                                   | object  | {}                                                          |
| labelCol       | AFormItem的labelCol                                                    | object  | {span:4}                                                    |
| wrapperCol     | AFormItem的wrapperCol                                                  | object  | {span:11}                                                   |
| isRequired     | 是否必选                                                               | boolean | true                                                        |
| multiple       | 是否多选                                                               | boolean | false                                                       |
| isTitle        | 是否显示标题                                                           | boolean | true                                                        |
| placeholder    | 树选择框预览文字                                                       | string  | 请选择                                                      |
| title          | AFormItem的标题                                                        | string  | 耗材配件类型                                                |
| name           | AFormItem的name，用于AForm检测是否必填                                 | string  | accessory                                                   |

**Event**

| 事件名称 | 说明                   | 回调参数               |
| -------- | ---------------------- | ---------------------- |
| change   | 选中树节点时调用此函数 | function(value:string) |

#### IVAssetsTypeSelector

**说明**

耗材资产类型，树形下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                                                   | 类型    | 默认值                                                      |
| -------------- | ---------------------------------------------------------------------- | ------- | ----------------------------------------------------------- |
| value(v-model) | 指定当前选中的条目                                                     | any     |                                                             |
| params         | 接口请求参数（get方式）                                                | object  | {}                                                          |
| data           | 接口请求参数（post方式）                                               | object  | {}                                                          |
| replaceFields  | 替换 treeNode 中 label,value,key,children 字段为 treeData 中对应的字段 | object  | {children:'children', label:'name', key:'id', value: 'id' } |
| inputStyle     | 树选择框的style样式                                                    | object  | {}                                                          |
| formItemStyle  | AFormItem的style样式                                                   | object  | {}                                                          |
| labelCol       | AFormItem的labelCol                                                    | object  | {span:4}                                                    |
| wrapperCol     | AFormItem的wrapperCol                                                  | object  | {span:11}                                                   |
| isRequired     | 是否必选                                                               | boolean | true                                                        |
| multiple       | 是否多选                                                               | boolean | false                                                       |
| isTitle        | 是否显示标题                                                           | boolean | true                                                        |
| placeholder    | 树选择框预览文字                                                       | string  | 请选择                                                      |
| title          | AFormItem的标题                                                        | string  | 资产类型                                                    |
| name           | AFormItem的name，用于AForm检测是否必填                                 | string  | accessory                                                   |

**Event**

| 事件名称 | 说明                   | 回调参数               |
| -------- | ---------------------- | ---------------------- |
| change   | 选中树节点时调用此函数 | function(value:string) |

#### IVMaterialTypeDropdown

**说明**

材料类型类型，树形下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                                                   | 类型    | 默认值                                                                               |
| -------------- | ---------------------------------------------------------------------- | ------- | ------------------------------------------------------------------------------------ |
| value(v-model) | 指定当前选中的条目                                                     | any     |                                                                                      |
| params         | 接口请求参数（get方式）                                                | object  | {}                                                                                   |
| data           | 接口请求参数（post方式）                                               | object  | {}                                                                                   |
| replaceFields  | 替换 treeNode 中 label,value,key,children 字段为 treeData 中对应的字段 | object  | {children:'childs', label:'name', key:'id', value: 'id'，selectable: "selectable", } |
| inputStyle     | 树选择框的style样式                                                    | object  | {}                                                                                   |
| formItemStyle  | AFormItem的style样式                                                   | object  | {}                                                                                   |
| labelCol       | AFormItem的labelCol                                                    | object  | {span:4}                                                                             |
| wrapperCol     | AFormItem的wrapperCol                                                  | object  | {span:11}                                                                            |
| isRequired     | 是否必选                                                               | boolean | true                                                                                 |
| multiple       | 是否多选                                                               | boolean | false                                                                                |
| isTitle        | 是否显示标题                                                           | boolean | true                                                                                 |
| placeholder    | 树选择框预览文字                                                       | string  | 请选择                                                                               |
| title          | AFormItem的标题                                                        | string  | 材料类型                                                                             |
| name           | AFormItem的name，用于AForm检测是否必填                                 | string  | materialCategoryId                                                                   |

**Event**

| 事件名称 | 说明                   | 回调参数               |
| -------- | ---------------------- | ---------------------- |
| change   | 选中树节点时调用此函数 | function(value:string) |

#### IVMaterialTypeSelector

**说明**

材料类型，树形下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                                                   | 类型    | 默认值                                                    |
| -------------- | ---------------------------------------------------------------------- | ------- | --------------------------------------------------------- |
| value(v-model) | 指定当前选中的条目                                                     | any     |                                                           |
| params         | 接口请求参数（get方式）                                                | object  | {}                                                        |
| data           | 接口请求参数（post方式）                                               | object  | {}                                                        |
| replaceFields  | 替换 treeNode 中 label,value,key,children 字段为 treeData 中对应的字段 | object  | {children:'childs', label:'name', key:'id', value: 'id' } |
| inputStyle     | 树选择框的style样式                                                    | object  | {}                                                        |
| formItemStyle  | AFormItem的style样式                                                   | object  | {}                                                        |
| labelCol       | AFormItem的labelCol                                                    | object  | {span:4}                                                  |
| wrapperCol     | AFormItem的wrapperCol                                                  | object  | {span:11}                                                 |
| isRequired     | 是否必选                                                               | boolean | true                                                      |
| multiple       | 是否多选                                                               | boolean | false                                                     |
| isTitle        | 是否显示标题                                                           | boolean | true                                                      |
| placeholder    | 树选择框预览文字                                                       | string  | 请选择                                                    |
| title          | AFormItem的标题                                                        | string  | 材料类型                                                  |
| name           | AFormItem的name，用于AForm检测是否必填                                 | string  | materialCategoryId                                        |
| sharecool      | 更改接口（细节自行查看）                                               | boolean | false                                                     |

**Event**

| 事件名称 | 说明                   | 回调参数                      |
| -------- | ---------------------- | ----------------------------- |
| change   | 选中树节点时调用此函数 | function(value, label, extra) |

#### IVOrgDropdown

**说明**

所属组织，树形下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                                                   | 类型    | 默认值                                                             |
| -------------- | ---------------------------------------------------------------------- | ------- | ------------------------------------------------------------------ |
| value(v-model) | 指定当前选中的条目                                                     | any     |                                                                    |
| params         | 接口请求参数（get方式）                                                | object  | {}                                                                 |
| data           | 接口请求参数（post方式）                                               | object  | {}                                                                 |
| replaceFields  | 替换 treeNode 中 label,value,key,children 字段为 treeData 中对应的字段 | object  | {children:'children', label:'displayName', key:'id', value: 'id' } |
| inputStyle     | 树选择框的style样式                                                    | object  | { width: "300px",}                                                 |
| formItemStyle  | AFormItem的style样式                                                   | object  | {}                                                                 |
| labelCol       | AFormItem的labelCol                                                    | object  | {span:4}                                                           |
| wrapperCol     | AFormItem的wrapperCol                                                  | object  | {span:11}                                                          |
| isRequired     | 是否必选                                                               | boolean | true                                                               |
| multiple       | 是否多选                                                               | boolean | false                                                              |
| isTitle        | 是否显示标题                                                           | boolean | true                                                               |
| placeholder    | 树选择框预览文字                                                       | string  | 请选择                                                             |
| title          | AFormItem的标题                                                        | string  | 所属组织                                                           |
| isByUser       | 是否使用ByUser结尾的接口                                               | boolean | true                                                               |

**Event**

| 事件名称 | 说明                   | 回调参数                      |
| -------- | ---------------------- | ----------------------------- |
| change   | 选中树节点时调用此函数 | function(value, label, extra) |

### 下拉框选择

#### IVInnerContractSelector

**说明**

所属分包合同类型，下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                    | 类型                                           | 默认值                      |
| -------------- | --------------------------------------- | ---------------------------------------------- | --------------------------- |
| value(v-model) | 指定当前选中的条目                      | any                                            |                             |
| params         | 接口请求参数（get方式）                 | object                                         | {}                          |
| data           | 接口请求参数（post方式）                | object                                         | {}                          |
| fieldNames     | 自定义节点 label、value、options 的字段 | {label: string,value: string,options?: object} | {label:'name',value: 'id' } |
| inputStyle     | 树选择框的style样式                     | object                                         | {width: "100%",}            |
| formItemStyle  | AFormItem的style样式                    | object                                         | {}                          |
| labelCol       | AFormItem的labelCol                     | object                                         | {}                          |
| wrapperCol     | AFormItem的wrapperCol                   | object                                         | {span:8}                    |
| isRequired     | 是否必选                                | boolean                                        | true                        |
| multiple       | 是否多选                                | boolean                                        | true                        |
| isTitle        | 是否显示标题                            | boolean                                        | true                        |
| placeholder    | 树选择框预览文字                        | string                                         | 请选择合同                  |
| title          | AFormItem的标题                         | string                                         | 所属分包合同                |

**Event**

| 事件名称 | 说明                 | 回调参数               |
| -------- | -------------------- | ---------------------- |
| change   | 选中节点时调用此函数 | function(value:string) |

#### IVMaterialTypeDropdownFilter

**说明**

材料类型过滤版本，下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                    | 类型                                            | 默认值                      |
| -------------- | --------------------------------------- | ----------------------------------------------- | --------------------------- |
| value(v-model) | 指定当前选中的条目                      | any                                             |                             |
| params         | 接口请求参数（get方式）                 | { contractId: string; [propName: string]: any } | {}                          |
| data           | 接口请求参数（post方式）                | object                                          | {}                          |
| fieldNames     | 自定义节点 label、value、options 的字段 | {label: string,value: string,options?: object}  | {label:'name',value: 'id' } |
| inputStyle     | 树选择框的style样式                     | object                                          | {width: "100%",}            |
| formItemStyle  | AFormItem的style样式                    | object                                          | {}                          |
| labelCol       | AFormItem的labelCol                     | object                                          | {span:4}                    |
| wrapperCol     | AFormItem的wrapperCol                   | object                                          | {span:11}                   |
| isRequired     | 是否必选                                | boolean                                         | true                        |
| multiple       | 是否多选                                | boolean                                         | false                       |
| isTitle        | 是否显示标题                            | boolean                                         | true                        |
| placeholder    | 树选择框预览文字                        | string                                          | 请选择                      |
| title          | AFormItem的标题                         | string                                          | 材料类型                    |
| name           | AFormItem的name，用于AForm检测是否必填  | string                                          | materialCategoryId          |

**Event**

| 事件名称 | 说明                 | 回调参数               |
| -------- | -------------------- | ---------------------- |
| change   | 选中节点时调用此函数 | function(value:string) |

#### IVLineDropdown

**说明**

所属分包合同类型，下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                    | 类型                                           | 默认值                             |
| -------------- | --------------------------------------- | ---------------------------------------------- | ---------------------------------- |
| value(v-model) | 指定当前选中的条目                      | any                                            |                                    |
| params         | 接口请求参数（get方式）                 | object                                         | {}                                 |
| data           | 接口请求参数（post方式）                | object                                         | {}                                 |
| fieldNames     | 自定义节点 label、value、options 的字段 | {label: string,value: string,options?: object} | {label:'displayName',value: 'id' } |
| inputStyle     | 树选择框的style样式                     | object                                         | {width: "100%",}                   |
| formItemStyle  | AFormItem的style样式                    | object                                         | {}                                 |
| labelCol       | AFormItem的labelCol                     | object                                         | {}                                 |
| wrapperCol     | AFormItem的wrapperCol                   | object                                         | {span:8}                           |
| isRequired     | 是否必选                                | boolean                                        | true                               |
| multiple       | 是否多选                                | boolean                                        | true                               |
| isTitle        | 是否显示标题                            | boolean                                        | true                               |
| placeholder    | 树选择框预览文字                        | string                                         | 请选择                             |
| title          | AFormItem的标题                         | string                                         | 所属条线                           |

**Event**

| 事件名称 | 说明                 | 回调参数               |
| -------- | -------------------- | ---------------------- |
| change   | 选中节点时调用此函数 | function(value:string) |

#### IVMainContractSelector

**说明**

所属分包合同类型，下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                    | 类型                                           | 默认值                      |
| -------------- | --------------------------------------- | ---------------------------------------------- | --------------------------- |
| value(v-model) | 指定当前选中的条目                      | any                                            |                             |
| params         | 接口请求参数（get方式）                 | object                                         | {}                          |
| data           | 接口请求参数（post方式）                | object                                         | {}                          |
| fieldNames     | 自定义节点 label、value、options 的字段 | {label: string,value: string,options?: object} | {label:'name',value: 'id' } |
| inputStyle     | 树选择框的style样式                     | object                                         | {width: "100%",}            |
| formItemStyle  | AFormItem的style样式                    | object                                         | {}                          |
| labelCol       | AFormItem的labelCol                     | object                                         | {}                          |
| wrapperCol     | AFormItem的wrapperCol                   | object                                         | {span:8}                    |
| isRequired     | 是否必选                                | boolean                                        | true                        |
| multiple       | 是否多选                                | boolean                                        | false                       |
| isTitle        | 是否显示标题                            | boolean                                        | true                        |
| placeholder    | 树选择框预览文字                        | string                                         | 请选择工程项目合同          |
| title          | AFormItem的标题                         | string                                         | 所属工程项目合同            |
| name           | AFormItem的name，用于AForm检测是否必填  | string                                         | parentContractId            |

**Event**

| 事件名称 | 说明                 | 回调参数               |
| -------- | -------------------- | ---------------------- |
| change   | 选中节点时调用此函数 | function(value:string) |

#### IVManagDropdown

**说明**

所属分包合同类型，下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                    | 类型                                           | 默认值                      |
| -------------- | --------------------------------------- | ---------------------------------------------- | --------------------------- |
| value(v-model) | 指定当前选中的条目                      | any                                            |                             |
| params         | 接口请求参数（get方式）                 | object                                         | {}                          |
| data           | 接口请求参数（post方式）                | object                                         | {}                          |
| fieldNames     | 自定义节点 label、value、options 的字段 | {label: string,value: string,options?: object} | {label:'name',value: 'id' } |
| inputStyle     | 树选择框的style样式                     | object                                         | {}                          |
| formItemStyle  | AFormItem的style样式                    | object                                         | {}                          |
| labelCol       | AFormItem的labelCol                     | object                                         | {}                          |
| wrapperCol     | AFormItem的wrapperCol                   | object                                         | {}                          |
| isRequired     | 是否必选                                | boolean                                        | true                        |
| multiple       | 是否多选                                | boolean                                        | false                       |
| isTitle        | 是否显示标题                            | boolean                                        | true                        |
| placeholder    | 树选择框预览文字                        | string                                         | 请选择                      |
| title          | AFormItem的标题                         | string                                         | 管理主体                    |
| name           | AFormItem的name，用于AForm检测是否必填  | string                                         | }                           |

**Event**

| 事件名称 | 说明                 | 回调参数               |
| -------- | -------------------- | ---------------------- |
| change   | 选中节点时调用此函数 | function(value:string) |

#### IVOuterContractSelector

**说明**

所属分包合同，下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                    | 类型                                           | 默认值                      |
| -------------- | --------------------------------------- | ---------------------------------------------- | --------------------------- |
| value(v-model) | 指定当前选中的条目                      | any                                            |                             |
| params         | 接口请求参数（get方式）                 | object                                         | {}                          |
| data           | 接口请求参数（post方式）                | object                                         | {}                          |
| fieldNames     | 自定义节点 label、value、options 的字段 | {label: string,value: string,options?: object} | {label:'name',value: 'id' } |
| inputStyle     | 树选择框的style样式                     | object                                         | {width: "100%",}            |
| formItemStyle  | AFormItem的style样式                    | object                                         | {}                          |
| labelCol       | AFormItem的labelCol                     | object                                         | {}                          |
| wrapperCol     | AFormItem的wrapperCol                   | object                                         | {span: 8,}                  |
| isRequired     | 是否必选                                | boolean                                        | true                        |
| multiple       | 是否多选                                | boolean                                        | true                        |
| isTitle        | 是否显示标题                            | boolean                                        | true                        |
| placeholder    | 树选择框预览文字                        | string                                         | 请选择合同                  |
| title          | AFormItem的标题                         | string                                         | 所属分包合同                |

**Event**

| 事件名称 | 说明                 | 回调参数               |
| -------- | -------------------- | ---------------------- |
| change   | 选中节点时调用此函数 | function(value:string) |

#### IVPostDropdown

**说明**

岗位，下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                    | 类型                                           | 默认值                      |
| -------------- | --------------------------------------- | ---------------------------------------------- | --------------------------- |
| value(v-model) | 指定当前选中的条目                      | any                                            |                             |
| params         | 接口请求参数（get方式）                 | object                                         | {}                          |
| data           | 接口请求参数（post方式）                | object                                         | {}                          |
| fieldNames     | 自定义节点 label、value、options 的字段 | {label: string,value: string,options?: object} | {label:'name',value: 'id' } |
| inputStyle     | 树选择框的style样式                     | object                                         | {width: "300px",}           |
| formItemStyle  | AFormItem的style样式                    | object                                         | {}                          |
| labelCol       | AFormItem的labelCol                     | object                                         | {}                          |
| wrapperCol     | AFormItem的wrapperCol                   | object                                         | {span: 8,}                  |
| isRequired     | 是否必选                                | boolean                                        | true                        |
| multiple       | 是否多选                                | boolean                                        | false                       |
| isTitle        | 是否显示标题                            | boolean                                        | true                        |
| placeholder    | 树选择框预览文字                        | string                                         | 请选择                      |
| title          | AFormItem的标题                         | string                                         | 岗位                        |

**Event**

| 事件名称 | 说明                 | 回调参数               |
| -------- | -------------------- | ---------------------- |
| change   | 选中节点时调用此函数 | function(value:string) |

#### IVProdBaseDropdown

**说明**

所属基地，下拉框，有AFormItem包裹。

**API**

| 参数           | 说明                                         | 类型                                           | 默认值                      |
| -------------- | -------------------------------------------- | ---------------------------------------------- | --------------------------- |
| value(v-model) | 指定当前选中的条目                           | any                                            |                             |
| params         | 接口请求参数（get方式）                      | object                                         | {}                          |
| data           | 接口请求参数（post方式）                     | object                                         | {}                          |
| fieldNames     | 自定义节点 label、value、options 的字段      | {label: string,value: string,options?: object} | {label:'name',value: 'id' } |
| inputStyle     | 树选择框的style样式                          | object                                         | {width: "100%",}            |
| formItemStyle  | AFormItem的style样式                         | object                                         | {}                          |
| labelCol       | AFormItem的labelCol                          | object                                         | {}                          |
| wrapperCol     | AFormItem的wrapperCol                        | object                                         | {span: 8,}                  |
| isRequired     | 是否必选                                     | boolean                                        | true                        |
| multiple       | 是否多选                                     | boolean                                        | true                        |
| isTitle        | 是否显示标题                                 | boolean                                        | true                        |
| placeholder    | 树选择框预览文字                             | string                                         | 请选择                      |
| title          | AFormItem的标题                              | string                                         | 所属基地                    |
| name           | AFormItem的name，用于AForm检测是否必填       | string                                         | productionBaseId            |
| isSetOfBookId  | 是否根据本地账套过滤基地（调用接口过滤）     | boolean                                        | false                       |
| setOfBookId    | 根据指定账套过滤基地，没有传递取账号当前账套 | string                                         |                             |

**Event**

| 事件名称 | 说明                 | 回调参数                          |
| -------- | -------------------- | --------------------------------- |
| change   | 选中节点时调用此函数 | function(value:string,option:any) |

#### IVSetofbookDropdown

**说明**

所属账套，下拉框，有AFormItem包裹。

**API**

| 参数                  | 说明                                                                                                                                                                              | 类型                                           | 默认值                                                      |
| --------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- | ----------------------------------------------------------- |
| value(v-model)        | 指定当前选中的条目                                                                                                                                                                | any                                            |                                                             |
| params                | 接口请求参数（get方式）                                                                                                                                                           | object                                         | {}                                                          |
| data                  | 接口请求参数（post方式）                                                                                                                                                          | object                                         | {}                                                          |
| fieldNames            | 自定义节点 label、value、options 的字段                                                                                                                                           | {label: string,value: string,options?: object} | {label:'name',value: 'id' }                                 |
| replaceFields         | 替换 treeNode 中 label,value,key,children 字段为 treeData 中对应的字段                                                                                                            | object                                         | {children:'children', label:'name', key:'id', value: 'id' } |
| inputStyle            | 树选择框的style样式                                                                                                                                                               | object                                         | {width: "300px",}                                           |
| formItemStyle         | AFormItem的style样式                                                                                                                                                              | object                                         | {}                                                          |
| labelCol              | AFormItem的labelCol                                                                                                                                                               | object                                         | {}                                                          |
| wrapperCol            | AFormItem的wrapperCol                                                                                                                                                             | object                                         | {span: 8,}                                                  |
| isRequired            | 是否必选                                                                                                                                                                          | boolean                                        | true                                                        |
| isDefault             | 是否去默认第一条数据为默认值                                                                                                                                                      | boolean                                        | false                                                       |
| multiple              | 是否多选                                                                                                                                                                          | boolean                                        | true                                                        |
| isTitle               | 是否显示标题                                                                                                                                                                      | boolean                                        | true                                                        |
| allowClear            | 是否显示清除按钮                                                                                                                                                                  | boolean                                        | true                                                        |
| isFilterUserSetOfBook | 是否根据用户权限筛选账套（生产模式）                                                                                                                                              | boolean                                        | false                                                       |
| placeholder           | 树选择框预览文字                                                                                                                                                                  | string                                         | 请选择                                                      |
| title                 | AFormItem的标题                                                                                                                                                                   | string                                         | 所属账套                                                    |
| name                  | AFormItem的name，用于AForm检测是否必填                                                                                                                                            | string                                         | setOfBookIds                                                |
| isSetOfBookId         | 是否根据本地账套过滤账套（生产模式）                                                                                                                                              | boolean                                        | false                                                       |
| setOfBookId           | 根据指定账套过滤基地，没有传递取账号当前账套，（经济模式下，默认根据当前账户账套筛选，可传入值。生产模式下，默认获取全部，传入isSetOfBookId则表示根据当前账户账套筛选，可传入值） | string                                         |                                                             |
| type                  | 区分生产和经济请求接口                                                                                                                                                            | "economic" \|"production"                      | "production"                                                |

**Event**

| 事件名称 | 说明                 | 回调参数                          |
| -------- | -------------------- | --------------------------------- |
| change   | 选中节点时调用此函数 | function(value:string,option:any) |

#### IVRateDropdown

**说明**

税率选择，下拉框，无AFormItem包裹。

**API**

| 参数           | 说明                                    | 类型                                           | 默认值                        |
| -------------- | --------------------------------------- | ---------------------------------------------- | ----------------------------- |
| value(v-model) | 指定当前选中的条目                      | any                                            |                               |
| fieldNames     | 自定义节点 label、value、options 的字段 | {label: string,value: string,options?: object} | {label:'name',value: 'name' } |
| inputStyle     | 树选择框的style样式                     | object                                         | {width: "100%",}              |

**Event**

| 事件名称 | 说明                 | 回调参数                          |
| -------- | -------------------- | --------------------------------- |
| change   | 选中节点时调用此函数 | function(value:string,option:any) |

#### IVUnitDropdown

**说明**

单位选择，下拉框，无AFormItem包裹。

**API**

| 参数           | 说明                                    | 类型                                           | 默认值                                                      |
| -------------- | --------------------------------------- | ---------------------------------------------- | ----------------------------------------------------------- |
| value(v-model) | 指定当前选中的条目                      | any                                            |                                                             |
| fieldNames     | 自定义节点 label、value、options 的字段 | {label: string,value: string,options?: object} | { label: "name", value: "id", options: "measurementUnits" } |
| inputStyle     | 树选择框的style样式                     | object                                         | {}                                                          |
| params         | 接口请求参数（get方式）                 | object                                         | {}                                                          |
| data           | 接口请求参数（post方式）                | object                                         | {}                                                          |
| bordered       | 是否显示边框                            | boolean                                        | false                                                       |
| placeholder    | 选择框预览文字                          | string                                         |                                                             |
| lineEnum       | 条线筛选                                | number                                         |                                                             |

**Event**

| 事件名称 | 说明                 | 回调参数                           |
| -------- | -------------------- | ---------------------------------- |
| change   | 选中节点时调用此函数 | function(value:string,name:string) |

#### IVPaytypeSelector

**说明**

付款形式，多选框，有AFormItem包裹。

**API**

| 参数           | 说明                     | 类型    | 默认值     |
| -------------- | ------------------------ | ------- | ---------- |
| value(v-model) | 指定当前选中的条目       | any     |            |
| params         | 接口请求参数（get方式）  | object  | {}         |
| data           | 接口请求参数（post方式） | object  | {}         |
| formItemStyle  | AFormItem的style样式     | object  | {}         |
| labelCol       | AFormItem的labelCol      | object  | {span: 2,} |
| wrapperCol     | AFormItem的wrapperCol    | object  | {span:16}  |
| isTitle        | 是否显示标题             | boolean | true       |
| title          | AFormItem的标题          | string  | 付款形式   |

**Event**

| 事件名称         | 说明                 | 回调参数               |
| ---------------- | -------------------- | ---------------------- |
| change（未使用） | 选中节点时调用此函数 | function(value:string) |

#### IVClientSelector

**说明**

甲方选择，弹窗显示。

**API**

| 参数          | 说明     | 类型    | 默认值 |
| ------------- | -------- | ------- | ------ |
| show(v-model) | 弹窗显示 | boolean |        |
| title         | 弹窗标题 | string  | 甲方   |

**Event**

| 事件名称 | 说明                                                        | 回调参数               |
| -------- | ----------------------------------------------------------- | ---------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string) |

### 弹窗选择

#### IVContractSelector

**说明**

合同选择，弹窗显示。

**API**

| 参数                   | 说明                                            | 类型     | 默认值 |
| ---------------------- | ----------------------------------------------- | -------- | ------ |
| show(v-model)          | 弹窗显示                                        | boolean  |        |
| isSearchShow           | 是否显示搜索                                    | boolean  | true   |
| isProjectShow          | 是否显示项目列                                  | boolean  | false  |
| isSetOfBook            | 是否显示账套搜索                                | boolean  | true   |
| isSignStatus           | 是否显示合同状态搜索                            | boolean  | true   |
| haskingDeeNo           | 是否显示金蝶列以及金蝶搜索                      | boolean  | false  |
| query                  | 接口请求参数（get方式）                         | object   | {}     |
| params                 | 接口请求参数（get方式）存在预留字段（自信查看） | object   | {}     |
| params.showAmount      | 是否显示金额列                                  | boolean  | false  |
| data                   | 接口请求参数（post方式）                        | object   | {}     |
| setOfBookIds           | 请求接口字段，会在请求数据携带                  | boolean  | false  |
| contractType           | 请求接口字段，会在请求数据携带                  | number   |        |
| businessTypeItems      | 请求接口字段，会在请求数据携带                  | number[] |        |
| notInBusinessTypeItems | 请求接口字段，会在请求数据携带                  | number[] |        |
| mainContractId         | 请求接口字段，会在请求数据携带                  | string   |        |
| partyAIds              | 请求接口字段，会在请求数据携带                  | number[] |        |
| partyBIds              | 请求接口字段，会在请求数据携带                  | number[] |        |

**Event**

| 事件名称 | 说明                                                        | 回调参数               |
| -------- | ----------------------------------------------------------- | ---------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string) |

#### IVMaterialSelector

**说明**

库存材料选择，弹窗显示。

**API**

| 参数          | 说明     | 类型    | 默认值 |
| ------------- | -------- | ------- | ------ |
| show(v-model) | 弹窗显示 | boolean |        |

**Event**

| 事件名称 | 说明                                                        | 回调参数               |
| -------- | ----------------------------------------------------------- | ---------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string) |

#### IVMeterageSelector

**说明**

验收单选择，弹窗显示。

**API**

| 参数                        | 说明                                                                  | 类型     | 默认值 |
| --------------------------- | --------------------------------------------------------------------- | -------- | ------ |
| show(v-model)               | 弹窗显示                                                              | boolean  |        |
| multiple                    | 是否多选                                                              | boolean  | false  |
| meterages                   | 多选编辑时回显数据                                                    | any[]    |        |
| list                        | 表格列表信息                                                          | any[]    |        |
| isMultipleCheckEmptyGetData | 多选时是否获取表单筛选数据，配合multipleFormData使用                  | boolean  |        |
| multipleFormData            | 多选时表单筛选数据                                                    | any      |        |
| closeMultipleLinkage        | 关闭多选时数据关联效果                                                | boolean  | false  |
| screenVendor                | 多选时，是否根据当前选择单据的信息进行筛选                            | boolean  | false  |
| screenMaterialCategory      | 多选时，是否根据当前选择单据的信息进行筛选                            | boolean  | false  |
| screenProductionBase        | 多选时，是否根据当前选择单据的信息进行筛选                            | boolean  | false  |
| screenPartyA                | 多选时，是否根据当前选择单据的信息进行筛选                            | boolean  | false  |
| screenPartyB                | 多选时，是否根据当前选择单据的信息进行筛选                            | boolean  | false  |
| screenContract              | 多选时，是否根据当前选择单据的信息进行筛选                            | boolean  | false  |
| contractQuery               | 弹窗内合同筛选参数                                                    | object   | {}     |
| moduleType                  | 模块类型（请求数据携带）                                              | number   |        |
| contractSale                | 合同sale参数（弹窗内合同筛选携带）                                    | number   |        |
| contractType                | 合同类型参数（弹窗内合同筛选携带）                                    | number   |        |
| accoutingType               | 单据类型，请求接口字段，会在请求数据携带，0：暂估、1：冲暂估、2：验收 | number   |        |
| productionBaseId            | 基地id，会在请求数据携带                                              | string   |        |
| businessType                | 请求接口字段，会在请求数据携带                                        | number   |        |
| setOfBookId                 | 请求接口字段，会在请求数据携带                                        | string   |        |
| materialCategoryId          | 请求接口字段，会在请求数据携带                                        | string   |        |
| projectId                   | 请求接口字段，会在请求数据携带                                        | string   |        |
| vendorId                    | 请求接口字段，会在请求数据携带                                        | string   |        |
| contractId                  | 请求接口字段，会在请求数据携带                                        | string   |        |
| partyAId                    | 请求接口字段，会在请求数据携带                                        | string   |        |
| partyBId                    | 请求接口字段，会在请求数据携带                                        | string   |        |
| managerId                   | 请求接口字段，会在请求数据携带                                        | string   |        |
| isEntry                     | 请求接口字段，会在请求数据携带                                        | boolean  | false  |
| notInBusinessTypeItems      | 请求接口字段，会在请求数据携带                                        | number[] |        |
| businessTypeItems           | 请求接口字段，会在请求数据携带                                        | number[] |        |

**Event**

| 事件名称 | 说明                                                        | 回调参数                               |
| -------- | ----------------------------------------------------------- | -------------------------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string,formData:object) |

#### IVPayContractSelector

**说明**

付款合同选择，弹窗显示。

**API**

| 参数                   | 说明                                            | 类型     | 默认值 |
| ---------------------- | ----------------------------------------------- | -------- | ------ |
| show(v-model)          | 弹窗显示                                        | boolean  |        |
| isSearchShow           | 是否显示搜索                                    | boolean  | true   |
| isProjectShow          | 是否显示项目列                                  | boolean  | false  |
| isSetOfBook            | 是否显示账套搜索                                | boolean  | true   |
| isSignStatus           | 是否显示合同状态搜索                            | boolean  | true   |
| query                  | 接口请求参数（get方式）                         | object   | {}     |
| params                 | 接口请求参数（get方式）存在预留字段（自信查看） | object   | {}     |
| data                   | 接口请求参数（post方式）                        | object   | {}     |
| setOfBookIds           | 请求接口字段，会在请求数据携带                  | boolean  | false  |
| contractType           | 请求接口字段，会在请求数据携带                  | number   |        |
| businessTypeItems      | 请求接口字段，会在请求数据携带                  | number[] |        |
| notInBusinessTypeItems | 请求接口字段，会在请求数据携带                  | number[] |        |
| mainContractId         | 请求接口字段，会在请求数据携带                  | string   |        |
| partyAIds              | 请求接口字段，会在请求数据携带                  | number[] |        |
| partyBIds              | 请求接口字段，会在请求数据携带                  | number[] |        |

**Event**

| 事件名称 | 说明                                                        | 回调参数               |
| -------- | ----------------------------------------------------------- | ---------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string) |

#### IVUserSelector

**说明**

人员选择，弹窗显示。

**API**

| 参数          | 说明             | 类型    | 默认值 |
| ------------- | ---------------- | ------- | ------ |
| show(v-model) | 弹窗显示         | boolean |        |
| lineEnum      | 条线筛选         | number  |        |
| propsFormData | 接口数据字段对象 | object  | {}     |

**Event**

| 事件名称 | 说明                                                        | 回调参数               |
| -------- | ----------------------------------------------------------- | ---------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string) |

#### IVVendorSelector

**说明**

供应商选择，弹窗显示。

**API**

| 参数          | 说明                   | 类型    | 默认值 |
| ------------- | ---------------------- | ------- | ------ |
| show(v-model) | 弹窗显示               | boolean |        |
| lineEnum      | 条线筛选               | number  |        |
| multiple      | 是否多选               | boolean | false  |
| isFinance     | 是否需要融资供应商筛选 | boolean | false  |
| venders       | 多选回显数据           | any[]   |        |
| query         | 请求携带参数对象       | object  | {}     |

**Event**

| 事件名称 | 说明                                                        | 回调参数               |
| -------- | ----------------------------------------------------------- | ---------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string) |

#### IVProjectModal

**说明**

项目选择，弹窗显示。

**API**

| 参数              | 说明                                             | 类型                             | 默认值   |
| ----------------- | ------------------------------------------------ | -------------------------------- | -------- |
| show(v-model)     | 弹窗显示                                         | boolean                          |          |
| multiple          | 弹窗标题                                         | boolean                          | false    |
| defaultData       | 弹窗回显数据，用于编辑是回显选中的数据，id的数组 | any[]                            | []       |
| query             | 接口请求参数（get方式）                          | object                           | {}       |
| title             | 弹窗标题                                         | string                           | 选择项目 |
| isSearchSetofbook | 是否显示账套筛选                                 | boolean                          | true     |
| isSearchProdBase  | 是否显示基地筛选                                 | boolean                          | true     |
| tableOptionFormat | 表格过滤函数                                     | (to: TableOption) => TableOption |          |

**Event**

| 事件名称 | 说明             | 回调参数                         |
| -------- | ---------------- | -------------------------------- |
| getItem  | 选中的数据时回调 | function(value:object\|objcet[]) |

#### IVContractAndProjectSelector(旧)

**说明**

项目选择，弹窗显示。

**API**

| 参数          | 说明                                   | 类型    | 默认值 |
| ------------- | -------------------------------------- | ------- | ------ |
| show(v-model) | 弹窗显示                               | boolean |        |
| multiple      | 弹窗标题                               | boolean | false  |
| defaultCheck  | 弹窗回显数据，用于编辑是回显选中的数据 | any[]   |        |
| query         | 接口请求参数（get方式）                | object  | {}     |

**Event**

| 事件名称 | 说明             | 回调参数                         |
| -------- | ---------------- | -------------------------------- |
| getItem  | 选中的数据时回调 | function(value:object\|objcet[]) |

#### IVProduceProjectSelector(旧)

**说明**

项目选择，弹窗显示。

**API**

| 参数          | 说明                                            | 类型    | 默认值 |
| ------------- | ----------------------------------------------- | ------- | ------ |
| show(v-model) | 弹窗显示                                        | boolean |        |
| isSearchShow  | 是否显示搜索                                    | boolean | true   |
| params        | 接口请求参数（get方式）存在预留字段（自信查看） | object  | {}     |
| data          | 接口请求参数（post方式）                        | object  | {}     |
| sale          | 请求接口字段，会在请求数据携带                  | boolean |        |
| setOfBookId   | 请求接口字段，会在请求数据携带                  | string  |        |
| moduleType    | 请求接口字段，会在请求数据携带                  | boolean |        |
| contractType  | 请求接口字段，会在请求数据携带                  | number  |        |
| contractId    | 请求接口字段，会在请求数据携带                  | string  |        |

**Event**

| 事件名称 | 说明                                                        | 回调参数               |
| -------- | ----------------------------------------------------------- | ---------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string) |

#### IVProjectSelector(旧)

**说明**

项目选择，弹窗显示。

**API**

| 参数             | 说明                                            | 类型    | 默认值 |
| ---------------- | ----------------------------------------------- | ------- | ------ |
| show(v-model)    | 弹窗显示                                        | boolean |        |
| isSearchShow     | 是否显示搜索                                    | boolean |        |
| multiple         | 是否多选（弃用）                                | boolean | false  |
| projects         | 多选回显数据（弃用）                            | any[]   |        |
| title            | 标题                                            | string  | 项目   |
| haskingDeeNo     | 是否显示金蝶编号筛选                            | boolean | false  |
| params           | 接口请求参数（get方式）存在预留字段（自信查看） | object  | {}     |
| data             | 接口请求参数（post方式）                        | object  | {}     |
| sale             | 请求接口字段，会在请求数据携带                  | boolean |        |
| setOfBookId      | 请求接口字段，会在请求数据携带                  | string  |        |
| moduleType       | 请求接口字段，会在请求数据携带                  | boolean |        |
| contractType     | 请求接口字段，会在请求数据携带                  | number  |        |
| contractId       | 请求接口字段，会在请求数据携带                  | string  |        |
| businessTypeItem | 请求接口字段，会在请求数据携带                  | string  |        |
| managerId        | 请求接口字段，会在请求数据携带                  | string  |        |
| baseId           | 请求接口字段，会在请求数据携带                  | string  |        |

**Event**

| 事件名称 | 说明                                                        | 回调参数               |
| -------- | ----------------------------------------------------------- | ---------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string) |

#### IVProjectsSelector(旧)

**说明**

项目选择，弹窗显示。

**API**

| 参数             | 说明                           | 类型    | 默认值 |
| ---------------- | ------------------------------ | ------- | ------ |
| show(v-model)    | 弹窗显示                       | boolean |        |
| multiple         | 是否多选（弃用）               | boolean | false  |
| projects         | 多选回显数据（弃用）           | any[]   |        |
| sale             | 请求接口字段，会在请求数据携带 | boolean |        |
| setOfBookId      | 请求接口字段，会在请求数据携带 | string  |        |
| moduleType       | 请求接口字段，会在请求数据携带 | boolean |        |
| contractType     | 请求接口字段，会在请求数据携带 | number  |        |
| contractId       | 请求接口字段，会在请求数据携带 | string  |        |
| businessTypeItem | 请求接口字段，会在请求数据携带 | string  |        |
| managerId        | 请求接口字段，会在请求数据携带 | string  |        |
| baseId           | 请求接口字段，会在请求数据携带 | string  |        |

**Event**

| 事件名称 | 说明                                                        | 回调参数               |
| -------- | ----------------------------------------------------------- | ---------------------- |
| getItem  | 选中的数据时回调，选中数据会经过stringify序列化，请自行转换 | function(value:string) |

### 其他控件

#### IVAdjustAmount

**说明**

金额合计及尾差，由不含税总价、总税额、总价以及对象尾差组成，数字输入框，有AFormItem包裹。

**API**

| 参数                           | 说明                   | 类型    | 默认值   |
| ------------------------------ | ---------------------- | ------- | -------- |
| untaxedAmount(v-model)         | 不含税总价             | number  |          |
| taxAmount(v-model)             | 总税额                 | number  |          |
| taxIncludedAmount(v-model)     | 总价                   | number  |          |
| tailUntaxedAmount(v-model)     | 不含税总价尾差         | number  |          |
| tailTaxAmount(v-model)         | 总税额尾差             | number  |          |
| tailTaxIncludedAmount(v-model) | 总价尾差               | number  |          |
| untaxedAmountDisabled          | 是否禁用不含税总价     | boolean | true     |
| taxAmountDisabled              | 是否禁用总税额         | boolean | true     |
| taxIncludedAmountDisabled      | 是否禁用总价           | boolean | true     |
| tailUntaxedAmountDisabled      | 是否禁用不含税总价尾差 | boolean | false    |
| tailTaxAmountDisabled          | 是否禁用总税额尾差     | boolean | false    |
| tailTaxIncludedAmountDisabled  | 是否禁用总价尾差       | boolean | false    |
| labelCol                       | AFormItem的labelCol    | object  | {span:4} |
| wrapperCol                     | AFormItem的wrapperCol  | object  | {span:8} |

**Event**

| 事件名称                      | 说明               | 回调参数               |
| ----------------------------- | ------------------ | ---------------------- |
| changeAdjustUntaxedAmount     | 不含税总价变化回调 | function(value:number) |
| changeAdjustTaxAmount         | 总税额变化回调     | function(value:number) |
| changeAdjustTaxIncludedAmount | 总价变化回调       | function(value:number) |

#### IVFileUpload

**说明**

文件上传，包含高拍仪显示。文件上传仅支持 "png","jpeg","gif","jpg","pdf","excel","csv","word"

**API**

| 参数               | 说明                                                                                                                                                                 | 类型                                                                                         | 默认值       |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ------------ |
| uploadApi          | 上传接口                                                                                                                                                             | Function                                                                                     |              |
| uploadLength       | 上传最大长度                                                                                                                                                         | number                                                                                       | 200          |
| fileList           | 上传数据列表，用于初始化回显图片                                                                                                                                     | string[]                                                                                     | []           |
| listType           | 上传图片列表样式，'picture-card'：图片卡片、'picture'：图片、'icon'：旧相机小图标 'diskIcon'：磁盘小图标、 'metronome'：高拍仪图标、'file'：文件、'textCustom'：文字 | 'picture-card' \| 'picture' \| 'icon' \| 'diskIcon' \| 'metronome' \| 'file' \| 'textCustom' | picture-card |
| showAltimeter      | 是否显示高拍仪按钮                                                                                                                                                   | boolean                                                                                      | true         |
| mini               | mini版组件                                                                                                                                                           | boolean                                                                                      | false        |
| uploadClass        | AUpload类型                                                                                                                                                          | object                                                                                       | {}           |
| showUploadList     | AUpload的showUploadList属性，上传预览                                                                                                                                | object                                                                                       | {}           |
| isShowFileSelecter | 是否显示文件选择器                                                                                                                                                   | boolean                                                                                      | false        |
| anyFileType        | 是否支持全部文件类型                                                                                                                                                 | object                                                                                       | {}           |

**Event**

| 事件名称        | 说明                                         | 回调参数                                                              |
| --------------- | -------------------------------------------- | --------------------------------------------------------------------- |
| cancelAltimeter | 关闭高拍仪弹窗                               | function(value:string)                                                |
| uploadChange    | 上传图片触发                                 | function({ file: string, fileList: string[],originFileList:object[]}) |
| altimeterUrl    | 高拍仪上传图片，返回上传图片路径以及文件信息 | function(url:string,result:object)                                    |

#### IVTimeSelector

**说明**

文件上传，包含高拍仪显示。文件上传仅支持 "png","jpeg","gif","jpg","pdf","excel","csv","word"

**API**

| 参数              | 说明                   | 类型     | 默认值                   |
| ----------------- | ---------------------- | -------- | ------------------------ |
| beginAt           | 开始时间               | string   |                          |
| endAt             | 结束时间               | string   |                          |
| placeholder       | 时间控件预览信息       | string[] | ["开始日期", "结束日期"] |
| activeBtn         | 按钮切换默认值         | number   | 0                        |
| isUtcMode         | 是否开始UTC日期模式    | boolean  | true                     |
| isDateRangePicker | 是否启用日期范围选择器 | boolean  | false                    |
| showGroup         | 是否显示左侧按钮组     | boolean  | true                     |
| timeType          | 弃用                   | Function |                          |

**Event**

| 事件名称     | 说明         | 回调参数                                   |
| ------------ | ------------ | ------------------------------------------ |
| selectedTime | 切换时间触发 | function(beginAt:string,endAt:string,type) |

#### ~~IVApproval（暂弃用）~~

**说明**

审核信息展示，无数据绑定。

**API**

| 参数  | 说明                     | 类型                               | 默认值 |
| ----- | ------------------------ | ---------------------------------- | ------ |
| data  | 审核展示数据             | customData（数据字段对照自行查看） | {}     |
| query | 提交审核参数（post方式） | object                             | {}     |

**Event**

| 事件名称 | 说明                                       | 回调参数             |
| -------- | ------------------------------------------ | -------------------- |
| success  | 提交审核成功回调，返回请求成功接口返回数据 | function(result:any) |
| close    | 点击返回按钮回调                           | function()           |
