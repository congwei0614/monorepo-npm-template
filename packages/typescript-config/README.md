# `@iv/typescript-config`

## base.json
详细解释这段配置中的各个部分：

## `compilerOptions`


#### baseUrl
- **作用**：设置解析非相对模块名称的基目录。
- **配置**：在这里设置为 `"."`，表示当前目录作为基准路径。

#### target
- **作用**：指定 ECMAScript 目标版本。
- **配置**：`"esnext"` 表示使用最新的 ECMAScript 版本。

#### useDefineForClassFields
- **作用**：启用新的 class 字段语义和初始化。
- **配置**：`true` 表示启用此特性，使用定义字段的标准行为。

#### module
- **作用**：指定生成哪个模块系统代码。
- **配置**：`"esnext"` 表示输出最新的 ECMAScript 模块。

#### moduleResolution
- **作用**：决定如何解析模块。
- **配置**：`"node"` 为 Node.js 模块解析机制。

#### strict
- **作用**：启用所有严格类型检查选项。
- **配置**：`true` 表示启用严格模式。

#### jsx
- **作用**：指定 JSX 代码生成的方式。
- **配置**：`"preserve"` 表示保留 JSX，交给后续的转换步骤处理。

#### sourceMap
- **作用**：生成相应的 `.map` 文件以支持调试。
- **配置**：`true` 表示启用源映射。

#### resolveJsonModule
- **作用**：允许导入 `.json` 文件作为模块。
- **配置**：`true` 表示可以导入 JSON 文件。

#### esModuleInterop
- **作用**：为了支持 CommonJS 和 ES 模块之间的互操作性。
- **配置**：`true` 使得 ES6 模块支持 `export =` 和 `import = require()`。

#### lib
- **作用**：指定要包含在编译中的库文件。
- **配置**：`["esnext", "dom"]` 包含最新 ECMAScript 特性和 DOM 类型定义。

#### skipLibCheck
- **作用**：跳过库文件的类型检查。
- **配置**：`true` 表示编译过程中不检查 `.d.ts` 文件的类型。

#### skipDefaultLibCheck
- **作用**：跳过对默认库声明的类型检查。
- **配置**：`true` 表示跳过默认库（如 `lib.d.ts`）的检查。

#### strictNullChecks
- **作用**：在严格检查模式下，`null` 和 `undefined` 值不包含在任何类型中除非显式指定。
- **配置**：`true` 表示启用严格的空值检查。

#### strictFunctionTypes
- **作用**：在函数类型的比较和赋值时采用严格模式。
- **配置**：`true` 表示函数类型将严格检查。

#### strictPropertyInitialization
- **作用**：确保类的非可选属性在构造函数中初始化。
- **配置**：`true` 表示类属性必须初始化。

#### useUnknownInCatchVariables
- **作用**：catch 语句中的异常变量使用 `unknown` 类型。
- **配置**：`false` 表示不强制使用 `unknown` 类型。

#### noImplicitThis
- **作用**：在不明确类型的上下文中使用 `this` 抛出错误。
- **配置**：`true` 表示禁止隐式的 `any` 类型的 `this`。

#### noUnusedLocals
- **作用**：报告未使用的局部变量。
- **配置**：`true` 表示编译器会在本地变量未使用时报错。

#### noUnusedParameters
- **作用**：报告未使用的函数参数。
- **配置**：`true` 表示函数参数未被使用时报错。

#### noImplicitReturns
- **作用**：在函数中每个分支都必须有返回值。
- **配置**：`true` 表示函数中每条路径都必须有返回值。

#### noFallthroughCasesInSwitch
- **作用**：防止 switch 语句的 case 贯穿。
- **配置**：`true` 表示每个 switch case 都需要一个 break 或 return 语句。

#### allowSyntheticDefaultImports
- **作用**：允许从没有默认导出的模块默认导入。
- **配置**：`true` 允许默认导入模拟。

#### experimentalDecorators
- **作用**：启用对 ECMAScript 装饰器的实验性支持。
- **配置**：`true` 表示启用装饰器。

#### emitDecoratorMetadata
- **作用**：在源码中包含装饰器元数据。
- **配置**：`true` 表示生成装饰器元数据，通常用于反射机制。


## web.json
详细解释这段配置中的各个部分：



#### extends
- **作用**：从另一个配置文件继承配置。
- **配置**：`"./base.json"` 表示此配置文件继承自当前目录下的 `base.json` 文件。这允许你共享和重用基本的 TypeScript 配置设置，减少重复配置。

#### compilerOptions.paths
- **作用**：配置模块的路径别名。
- **配置**：`{ "/@/*": ["./src/*"] }` 设置了一个路径别名 `/@/`，它映射到项目的 `src` 目录。这样，你可以在导入模块时使用类似 `import something from '/@/utils/my-util'` 的方式，直接指向 `src/utils/my-util`，而不需要使用相对路径。

#### include
- **作用**：指定 TypeScript 编译器应包括在编译过程中的文件或文件夹。
- **配置**：`["src/**/*.ts", "src/**/*.d.ts", "src/**/*.tsx", "src/**/*.vue"]` 这些 glob 模式告诉 TypeScript 编译器，应该包括 `src` 目录下的所有 `.ts`、`.d.ts`、`.tsx` 和 `.vue` 文件。这确保了所有相关文件都被考虑在内进行类型检查和编译。

#### exclude
- **作用**：指定在编译过程中应排除的文件或文件夹。
- **配置**：`["node_modules","dist"]` 这些路径是常见的排除项，表示 TypeScript 编译器在编译过程中应忽略 `node_modules` 和 `dist` 目录。`node_modules` 目录通常包含第三方库文件，而 `dist` 目录可能是你的构建输出目录，这些目录的文件不需要被 TypeScript 编译器处理。
