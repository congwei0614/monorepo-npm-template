# `@iv/eslint-config`

详细解释这段配置中的各个部分：

## `env`

`env`对象定义了代码运行的环境。这里指定了三个环境：

- `es6: true`：启用ES6语法支持。
- `browser: true`：启用浏览器全局变量。
- `node: true`：启用Node.js全局变量和模块。

## `plugins`

`plugins`数组列出了要使用的ESLint插件：

- `"@typescript-eslint"`：用于在TypeScript代码中执行ESLint规则。
- `"prettier"`：将Prettier的规则集成到ESLint中，以便在保存文件时自动格式化代码。
- `"unicorn"`：提供了一些独特的ESLint规则，用于进一步改进代码质量。

## `extends`

`extends`数组指定了继承哪些配置。这允许你重用其他配置中定义的规则，而无需重新编写它们。

- `"eslint:recommended"`：使用ESLint推荐的基本规则集。
- `"plugin:import/recommended"`：来自`eslint-plugin-import`的推荐规则，用于检查import语句。
- `"plugin:eslint-comments/recommended"`：来自`eslint-plugin-eslint-comments`的推荐规则，用于检查ESLint注释。
- `"plugin:jsonc/recommended-with-jsonc"`：来自`eslint-plugin-jsonc`的推荐规则，用于检查JSONC文件。
- `"plugin:vue/vue3-recommended"`：用于Vue 3的推荐规则集。
- `"plugin:@typescript-eslint/recommended"`：用于TypeScript的推荐规则集。
- `"prettier"`：将Prettier的规则集成到ESLint中，以确保代码格式的一致性。

## `settings`

`settings`对象提供了配置信息给插件使用。这里配置了`eslint-plugin-import`的解析器：

- `"import/resolver"`：定义了模块解析的方式。
  - `node`: 设置了Node.js模块的解析方式，并指定了额外的文件扩展名，如`.ts`, `.d.ts`, `.tsx`。

## `overrides`

### 第一个覆盖项

```json
{
  "files": ["*.json", "*.json5", "*.jsonc"],
  "parser": "jsonc-eslint-parser"
}
```

- `files`: 指定了这些规则应用于哪些文件，这里包括所有`.json`、`.json5`和`.jsonc`文件。
- `parser`: 使用`jsonc-eslint-parser`解析器来解析这些JSON文件，使得ESLint可以检查JSON文件的语法和格式。

### 第二个覆盖项

```json
{
  "files": ["*.ts", "*.vue"],
  "rules": {
    "no-undef": "off"
  }
}
```

- `files`: 这些规则应用于所有`.ts`（TypeScript文件）和`.vue`文件。
- `rules`:
  - `"no-undef": "off"`: 关闭了`no-undef`规则，这意味着在这些文件中，未定义的变量不会导致错误。这通常是因为TypeScript和Vue提供了自己的类型检查机制，所以不需要ESLint的`no-undef`规则。

### 第三个覆盖项

```json
{
  "files": ["**/__tests__/#### ],
  "rules": {
    "no-console": "off",
    "vue/one-component-per-file": "off"
  }
}
```

- `files`: 这些规则应用于所有在`__tests__`目录下的文件。
- `rules`:
  - `"no-console": "off"`: 关闭了`no-console`规则，这意味着在测试文件中使用`console`不会导致错误。这通常是因为在测试过程中，`console`语句经常用于输出调试信息。
  - `"vue/one-component-per-file": "off"`: 关闭了Vue的规则，允许一个文件中包含多个Vue组件。这在测试文件中可能是有用的，因为测试可能包含多个小的、辅助性的Vue组件。

### 第四个覆盖项

```json
{
  "files": ["package.json"],
  "parser": "jsonc-eslint-parser",
  "rules": {
    "jsonc/sort-keys": [
      "error",
      {
        "pathPattern": "^$",
        "order": [
          // ... key order configuration ...
        ]
      },
      {
        "pathPattern": "^(?:dev|peer|optional|bundled)?[Dd]ependencies$",
        "order": { "type": "asc" }
      }
    ]
  }
}
```

- `files`: 这些规则仅应用于`package.json`文件。
- `parser`: 使用`jsonc-eslint-parser`解析器来解析`package.json`文件。
- `rules`:
  - `"jsonc/sort-keys"`: 这个规则用于检查`package.json`中键的排序。它指定了不同部分的键应该如何排序。
    - 第一个配置对象指定了根级别的键的顺序。
    - 第二个配置对象特别针对依赖项（`dependencies`、`devDependencies`等）进行排序，按照字母顺序升序排列。

### 第五个覆盖项

```json
{
  "files": ["*.d.ts"],
  "rules": {
    "import/no-duplicates": "off"
  }
}
```

- `files`: 这些规则适用于所有`.d.ts`文件，即TypeScript的声明文件。
- `rules`:
  - `"import/no-duplicates": "off"`: 关闭了`import/no-duplicates`规则，这意味着在`.d.ts`文件中允许重复的import语句。在某些情况下，为了提供类型声明或覆盖现有的类型定义，可能需要重复的import。

### 第六个覆盖项

```json
{
  "files": ["*.js"],
  "rules": {
    "@typescript-eslint/no-var-requires": "off"
  }
}
```

- `files`: 这些规则适用于所有`.js`文件，即普通的JavaScript文件。
- `rules`:
  - `"@typescript-eslint/no-var-requires": "off"`: 关闭了`@typescript-eslint/no-var-requires`规则，这个规则通常用于TypeScript代码，以禁止使用`var require`语法，因为在TypeScript中更推荐使用`import`语法。但在普通的JavaScript文件中，使用`var require`是常见的做法，因此关闭这个规则是合理的。

### 第七个覆盖项

```json
{
  "files": ["*.vue"],
  "parser": "vue-eslint-parser",
  "parserOptions": {
    "parser": "@typescript-eslint/parser",
    "extraFileExtensions": [".vue"],
    "ecmaVersion": "latest",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "rules": {
    "no-undef": "off"
  }
}
```

- `files`: 这些规则适用于所有`.vue`文件，即Vue组件文件。
- `parser`: 使用`vue-eslint-parser`来解析Vue文件，它支持Vue文件的语法结构。
- `parserOptions`:
  - `parser`: 使用`@typescript-eslint/parser`来解析TypeScript代码，这意味着Vue文件中的`<script lang="ts">`部分将使用TypeScript的解析器进行解析。
  - `extraFileExtensions`: 指定额外的文件扩展名，这里添加了`.vue`，以确保解析器正确处理Vue文件。
  - `ecmaVersion`: 设置ECMAScript的版本为最新，以确保支持最新的JavaScript语法。
  - `ecmaFeatures`:
    - `jsx`: 设置为`true`，表示支持JSX语法，尽管这在Vue文件中可能不常见，但有时候Vue组件可能会包含一些JSX代码。
- `rules`:
  - `"no-undef": "off"`: 关闭了`no-undef`规则，这意味着在Vue文件的`<script>`部分中，未定义的变量不会导致错误。这可能是因为Vue组件经常依赖于Vue实例或组件的属性，而这些属性可能不会直接在脚本中定义。

## `rules`

### 通用JavaScript/TypeScript规则

#### camelcase

- **作用**：确保变量和函数名使用驼峰命名法。
- **配置**：允许对象属性不遵守驼峰命名法。

#### no-console

- **作用**：限制使用`console`。
- **配置**：允许使用`console.error`。

#### no-debugger

- **作用**：禁止使用`debugger`语句。

#### no-constant-condition

- **作用**：禁止在条件表达式中使用常量。
- **配置**：在循环中不检查常量条件。

#### no-restricted-syntax

- **作用**：禁止使用特定的语法结构。
- **配置**：禁止`LabeledStatement`和`WithStatement`。

#### no-return-await

- **作用**：禁止在返回语句中使用不必要的`await`。

#### no-var

- **作用**：禁止使用`var`声明变量，推荐使用`let`或`const`。

#### no-empty

- **作用**：禁止空代码块，但允许空的`catch`块。

#### prefer-const

- **作用**：优先使用`const`声明变量，而不是`let`。
- **配置**：解构时总是使用`const`，忽略在赋值前读取变量的情况。

#### prefer-arrow-callback

- **作用**：优先使用箭头函数作为回调函数。
- **配置**：不允许命名函数，允许在回调中不使用`this`。

#### object-shorthand

- **作用**：鼓励使用对象字面量的简写语法。
- **配置**：总是使用简写语法，不忽略构造函数，避免使用引号包裹属性名。

#### prefer-rest-params

- **作用**：优先使用剩余参数而不是`arguments`对象。

#### prefer-spread

- **作用**：优先使用扩展运算符而不是`apply`方法。

#### prefer-template

- **作用**：优先使用模板字符串而不是字符串拼接。

#### no-redeclare

- **作用**：禁止重复声明变量。
- **状态**：关闭此规则，因为下面启用了TypeScript特定的规则来检查重复声明。

#### @typescript-eslint/no-redeclare

- **作用**：在TypeScript中禁止重复声明变量。

### 最佳实践规则

#### array-callback-return

- **作用**：在数组方法的回调函数中返回结果。

#### block-scoped-var

- **作用**：要求所有变量声明使用块级作用域。

#### no-alert

- **作用**：禁止使用`alert`、`confirm`和`prompt`。

#### no-case-declarations

- **作用**：禁止在`switch`语句的`case`子句中使用声明。

#### no-multi-str

- **作用**：禁止使用多行字符串字面量。

#### no-with

- **作用**：禁止使用`with`语句。

#### no-void

- **作用**：禁止使用`void`操作符。

#### sort-imports

- **作用**：对导入语句进行排序。
- **配置**：详细定义了排序规则和分组设置。

### 样式问题规则

#### prefer-exponentiation-operator

- **作用**：优先使用指数运算符(`**`)而不是`Math.pow`。

### TypeScript特定规则

#### @typescript-eslint/explicit-module-boundary-types

- **作用**：要求函数和类具有明确的返回和参数类型。
- **状态**：关闭此规则。

#### @typescript-eslint/no-explicit-any

- **作用**：避免在类型注解中使用`any`类型。
- **状态**：关闭此规则。

#### @typescript-eslint/no-non-null-assertion

- **作用**：禁止使用非空断言操作符(`!`)。
- **状态**：关闭此规则。

#### @typescript-eslint/no-non-null-asserted-optional-chain

- **作用**：禁止在可选链后面使用非空断言。
- **状态**：关闭此规则。

#### @typescript-eslint/consistent-type-imports

- **作用**：确保类型导入的一致性。
- **配置**：允许在类型注解中使用类型导入。

#### @typescript-eslint/ban-ts-comment

- **作用**：禁止 TypeScript 特定的注释。
- **配置**：不允许使用 // @ts-ignore 注释。

### Vue 规则

#### vue/no-v-html

- **作用**：允许在 Vue 组件中使用 `v-html` 指令来插入 HTML 字符串。
- **配置**：关闭该规则，允许使用 `v-html`。

#### vue/require-default-prop

- **作用**：要求 Vue 组件的 props 必须有默认值。
- **配置**：关闭该规则，不要求 props 有默认值。

#### vue/require-explicit-emits

- **作用**：要求 Vue 组件显式声明它们会发出的事件。
- **配置**：关闭该规则，不要求组件显式声明发出的事件。

#### vue/multi-word-component-names

- **作用**：要求 Vue 组件名称必须为多单词。
- **配置**：关闭该规则，允许使用单词作为组件名。

#### vue/prefer-import-from-vue

- **作用**：优先从 `vue` 包中导入 API，而不是从其他来源。
- **配置**：关闭该规则，不强制从 `vue` 包导入 API。

#### vue/no-v-text-v-html-on-component

- **作用**：禁止在 Vue 组件上使用 `v-text` 和 `v-html` 指令。
- **配置**：关闭该规则，允许在组件上使用 `v-text` 和 `v-html`。

#### vue/html-self-closing

- **作用**：控制是否要求特定的 HTML、SVG 和 math 标签自闭合。
- **配置**：设置为错误级别，并强制 HTML 的 void、normal 和 component 标签，以及 SVG 和 math 标签始终自闭合。

### Prettier 规则

####prettier/prettier

- **作用**：强制使用 Prettier 的代码格式化规则。
- **配置**：设置为错误级别，违反 Prettier 规则的代码将触发错误。

### Import 规则

#### import/first

- **作用**：要求 import 语句必须是文件中的第一条语句。
- **配置**：设置为错误级别，违反该规则将触发错误。

#### import/no-duplicates

- **作用**：禁止在文件中重复导入相同的模块。
- **配置**：设置为错误级别，重复导入将触发错误。

#### import/order

- **作用**：定义 import 语句的顺序规则。
- **配置**：设置为错误级别，并定义了具体的分组和路径规则，违反这些规则将触发错误。

#### import/no-unresolved

- **作用**：禁止导入未解析的模块。
- **配置**：关闭该规则，允许导入未解析的模块。

#### import/namespace

- **作用**：相关的命名空间导入规则。
- **配置**：关闭该规则，不强制特定的命名空间导入规则。

#### import/default

- **作用**：相关的默认导入规则。
- **配置**：关闭该规则，不强制默认的导入规则。

#### import/no-named-as-default

- **作用**：禁止将具名导入作为默认导入。
- **配置**：关闭该规则，允许将具名导入作为默认导入。

#### import/no-named-as-default-member

- **作用**：禁止将具名导入作为默认导入的成员。
- **配置**：关闭该规则，允许将具名导入作为默认导入的成员。

#### import/named

- **作用**：相关的具名导入规则。
- **配置**：关闭该规则，不强制具名导入的规则。

### eslint-plugin-eslint-comments 规则

#### eslint-comments/disable-enable-pair

- **作用**：确保 ESLint 的 `/* eslint-disable */` 和 `/* eslint-enable */` 注释成对出现，避免代码片段意外地禁用或启用 ESLint 规则。
- **配置**：设置为错误级别，并允许整个文件级别的禁用/启用注释。

### unicorn 规则

#### unicorn/custom-error-definition

- **作用**：鼓励使用自定义错误类型而不是通用的 `Error` 类型，以提供更丰富的错误信息。
- **配置**：设置为错误级别，强制使用自定义错误定义。

#### unicorn/error-message

- **作用**：确保错误对象包含有意义的错误信息。
- **配置**：设置为错误级别，强制包含有效的错误信息。

#### unicorn/escape-case

- **作用**：要求使用正确的转义序列，例如 `\n` 而不是 `\x0A`。
- **配置**：设置为错误级别，强制使用标准的转义序列。

#### unicorn/import-index

- **作用**：限制从包的索引文件导入内容，鼓励直接导入所需的模块。
- **配置**：设置为错误级别，强制直接导入。

#### unicorn/new-for-builtins

- **作用**：对于内置对象（如 `Array`、`Object` 等），鼓励使用字面量语法而非构造函数。
- **配置**：设置为错误级别，强制使用字面量语法。

#### unicorn/no-array-method-this-argument

- **作用**：禁止在数组方法（如 `map`、`filter` 等）中使用不必要的 `this` 参数。
- **配置**：设置为错误级别，禁止使用不必要的 `this` 参数。

#### unicorn/no-array-push-push

- **作用**：避免使用多个连续的 `push` 调用，鼓励使用扩展运算符或 `concat` 方法。
- **配置**：设置为错误级别，禁止连续使用 `push`。

#### unicorn/no-console-spaces

- **作用**：禁止在 `console.log` 等方法中使用多余的空格。
- **配置**：设置为错误级别，避免在 `console` 方法中使用多余的空格。

#### unicorn/no-for-loop

- **作用**：鼓励使用更现代的循环结构（如 `for...of`），避免使用传统的 `for` 循环。
- **配置**：设置为错误级别，禁止使用 `for` 循环。

####

\*\*unicorn/no-hex-escape

- **作用**：禁止使用十六进制转义序列，因为它们可能导致混淆和可读性下降。
- **配置**：设置为错误级别，禁止使用十六进制转义。

#### unicorn/no-instanceof-array

- **作用**：推荐使用 `Array.isArray` 而不是 `instanceof Array` 来检查一个值是否是数组。
- **配置**：设置为错误级别，强制使用 `Array.isArray`。

#### unicorn/no-invalid-remove-event-listener

- **作用**：确保在调用 `removeEventListener` 时提供正确的参数，避免移除错误的事件监听器。
- **配置**：设置为错误级别，确保正确移除事件监听器。

#### unicorn/no-new-array

- **作用**：避免使用 `new Array()`，鼓励使用数组字面量。
- **配置**：设置为错误级别，强制使用数组字面量。

#### unicorn/no-new-buffer

- **作用**：避免使用 `new Buffer()`，因为它在旧版本的 Node.js 中存在安全隐患。
- **配置**：设置为错误级别，禁止使用 `new Buffer()`。

#### unicorn/no-unsafe-regex

- **作用**：禁止创建可能不安全的正则表达式，避免潜在的 ReDoS（正则表达式拒绝服务）攻击。
- **配置**：关闭该规则，允许创建可能不安全的正则表达式（请注意，这可能会增加安全风险）。

#### unicorn/number-literal-case

- **作用**：确保数字字面量使用小写形式，提高代码的一致性和可读性。
- **配置**：当代码中的数字字面量使用了大写形式时，将此视为一个错误。

#### unicorn/prefer-array-find

- **作用**：推荐使用数组的 `find` 方法来查找满足条件的元素，提高代码的可读性和简洁性。
- **配置**：当代码中未使用 `find` 方法来查找数组元素时，将此视为一个错误。

#### unicorn/prefer-array-flat-map

- **作用**：推荐使用 `Array.prototype.flatMap` 方法来同时执行映射和扁平化操作，简化代码逻辑。
- **配置**：当代码中未使用 `flatMap` 方法来同时映射和扁平化数组时，将此视为一个错误。

#### unicorn/prefer-array-index-of

- **作用**：推荐使用数组的 `indexOf` 方法来查找元素的索引，提高代码的可读性。
- **配置**：当代码中未使用 `indexOf` 方法来查找元素索引时，将此视为一个错误。

#### unicorn/prefer-array-some

- **作用**：推荐使用数组的 `some` 方法来检查是否有元素满足条件，提高代码的可读性。
- **配置**：当代码中未使用 `some` 方法来检查数组元素条件时，将此视为一个错误。

#### unicorn/prefer-date-now

- **作用**：推荐使用 `Date.now()` 来获取当前时间戳，简化代码并提高效率。
- **配置**：当代码中未使用 `Date.now()` 来获取时间戳时，将此视为一个错误。

#### unicorn/prefer-dom-node-dataset

- **作用**：推荐使用 DOM 节点的 `dataset` 属性来访问 `data-*` 属性，提高代码的可读性和维护性。
- **配置**：当代码中未使用 `dataset` 属性来访问 `data-*` 属性时，将此视为一个错误。

#### unicorn/prefer-includes

- **作用**：推荐使用 `includes` 方法来检查数组或字符串是否包含特定元素或子串，提高代码的可读性。
- **配置**：当代码中未使用 `includes` 方法进行检查时，将此视为一个错误。

#### unicorn/prefer-keyboard-event-key

- **作用**：推荐使用 `KeyboardEvent.key` 属性来获取按键信息，提高代码的准确性和一致性。
- **配置**：当代码中未使用 `key` 属性来获取按键信息时，将此视为一个错误。

#### unicorn/prefer-math-trunc

- **作用**：推荐使用 `Math.trunc` 方法来截断数字为整数，提高代码的简洁性和可读性。
- **配置**：当代码中未使用 `Math.trunc` 方法来截断数字时，将此视为一个错误。

#### unicorn/prefer-modern-dom-apis

- **作用**：推荐使用现代的 DOM API，以提高代码效率和可读性。
- **配置**：当代码中未使用推荐的现代 DOM API 时，将此视为一个错误。

#### unicorn/prefer-negative-index

- **作用**：推荐使用负索引来访问数组或字符串的末尾元素，简化操作。
- **配置**：当代码中未使用负索引来访问末尾元素时，将此视为一个错误。

#### unicorn/prefer-number-properties

- **作用**：推荐使用数字对象的属性来访问特定的数值常量，提高代码的可读性。
- **配置**：当代码中未使用数字对象的属性来访问数值常量时，将此视为一个错误。

#### unicorn/prefer-optional-catch-binding

- **作用**：推荐在 `catch` 子句中省略错误对象绑定，当不需要错误对象时，提高代码的简洁性。
- **配置**：当代码中在 `catch` 子句中未省略不必要的错误对象绑定时，将此视为一个错误。

#### unicorn/prefer-prototype-methods

- **作用**：推荐使用原型链上的方法来操作对象，以避免在实例上重复定义方法，从而优化内存使用和代码的可维护性。
- **配置**：当代码中直接在实例上定义方法，而没有利用原型链时，将此视为一个错误。

#### unicorn/prefer-query-selector

- **作用**：推荐使用 `querySelector` 或 `querySelectorAll` 方法来查询 DOM 元素，因为这种方法更灵活，且易于理解和维护。
- **配置**：当代码中使用了其他不够灵活的 DOM 查询方法时，将此视为一个错误。

#### unicorn/prefer-reflect-apply

- **作用**：推荐使用 `Reflect.apply` 方法来动态调用函数，因为这种方法提供了更灵活的参数处理机制。
- **配置**：当代码中未使用 `Reflect.apply` 方法来调用函数时，将此视为一个错误。

#### unicorn/prefer-string-slice

- **作用**：推荐使用 `String.prototype.slice` 方法来提取字符串的子串，因为它提供了更清晰的语义和更直观的使用方式。
- **配置**：当代码中未使用 `slice` 方法来提取字符串子串时，将此视为一个错误。

#### unicorn/prefer-string-starts-ends-with

- **作用**：推荐使用 `String.prototype.startsWith` 和 `String.prototype.endsWith` 方法来检查字符串是否以特定子串开始或结束，这些方法提供了更直观和易读的语义。
- **配置**：当代码中未使用 `startsWith` 或 `endsWith` 方法进行字符串检查时，将此视为一个错误。

#### unicorn/prefer-string-trim-start-end

- **作用**：推荐使用 `String.prototype.trimStart` 和 `String.prototype.trimEnd` 方法来去除字符串开头或结尾的空格，这些方法提供了更明确的操作意图。
- **配置**：当代码中未使用 `trimStart` 或 `trimEnd` 方法来去除空格时，将此视为一个错误。

#### unicorn/prefer-type-error

- **作用**：推荐在需要抛出错误时，使用 `TypeError` 而不是其他类型的错误，以更准确地描述错误的类型。
- **配置**：当代码中抛出了非 `TypeError` 类型的错误时，将此视为一个错误。

#### unicorn/throw-new-error

- **作用**：推荐在抛出错误时使用 `new Error()` 语法来创建错误对象，以确保错误对象具有正确的原型链和属性。
- **配置**：当代码中未使用 `new Error()` 语法来抛出错误时，将此视为一个错误。
