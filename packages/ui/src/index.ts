/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-08-08 18:47:06
 * @LastEditTime: 2024-04-16 11:55:30
 */
import { IVImageView, IVModal, IVPagination, IVTable, IVTableControl } from './basic/index'
import { IVSetofbookTreeSelector, IVVendorModal } from './business/index'
import type { App } from 'vue'

// 并没有引入任何样式，包括antv，tailwindcss等，因为会使用项目的样式元素。

const components: any[] = [
  IVImageView,
  IVModal,
  IVPagination,
  IVTable,
  IVTableControl,
  IVSetofbookTreeSelector,
  IVVendorModal
]

installComponents()

// 单独引用
function installComponents() {
  components.forEach(component => {
    component.install = function (app: App) {
      if (!component.__name) {
        console.error(`From iv-npm, Component missing __name property:`, component)
        return
      }
      app.component(component.__name, component)
    }
  })
}
export { IVImageView, IVModal, IVPagination, IVTable, IVTableControl, IVSetofbookTreeSelector, IVVendorModal }

// 全局引用
const install = (app: App): App => {
  components.forEach((component: any) => {
    if (!component.__name) {
      console.error(`From iv-npm, Component missing __name property:`, component)
      return
    }

    app.component(component?.__name, component)
  })
  return app
}
const cAll = { install }
export default cAll
