import { request } from '@iv/shared'

// 获取人员
export const getUserAll = (data?: any) => {
  return request<any, any>({
    url: '/api/basis-management/vendor',
    method: 'get',
    data,
    requestBase: 'BASIS_URL'
  })
}
