export const tableOption = [
  {
    title: '序号',
    w: `${(54 / 1499) * 100}%`,
    key: 'index',
    show: true
  },
  {
    title: '编号',
    w: `${(104 / 1499) * 100}%`,
    key: 'code',
    show: true
  },
  {
    title: '所属类别',
    w: `${(212 / 1499) * 100}%`,
    key: 'vendorTypes',
    show: true,
    custom: true
  },
  {
    title: '所属条线',
    w: `${(226 / 1499) * 100}%`,
    key: 'lines',
    show: true,
    custom: true
  },
  {
    title: '级别',
    w: `${(150 / 1499) * 100}%`,
    key: 'vendorLevel',
    show: true,
    custom: true
  },
  {
    title: '供应商名称',
    w: `${(250 / 1499) * 100}%`,
    key: 'name',
    show: true,
    custom: true
  },
  {
    title: '手机号码',
    w: `${(160 / 1499) * 100}%`,
    key: 'phoneNumber',
    show: true
  },
  {
    title: '',
    w: `${(45 / 1333) * 100}%`,
    key: 'buttonSlot',
    show: true
  }
]
