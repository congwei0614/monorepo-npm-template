export interface FieldNames {
  label: string
  value: string
  options?: object
}
