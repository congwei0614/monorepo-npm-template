/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-08-08 18:47:39
 * @LastEditTime: 2022-08-08 18:51:04
 */
/// <reference types="vite/client" />

declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}
interface Window {
  [propName: string]: any
}
