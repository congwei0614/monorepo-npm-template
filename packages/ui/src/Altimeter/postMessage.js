// 监听父级页面传递的数据
window.addEventListener('message', function (e) {
  console.log(e.data, '父级页面传来的数据')
})

let message = '来自iframe的数据'
// 向父级页面传递数据
window.parent.postMessage(message, '*')

