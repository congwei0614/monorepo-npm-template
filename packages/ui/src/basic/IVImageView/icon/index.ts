import PPTIcon from './PPT-icon.vue'
import WORDIcon from './WORD-icon.vue'
import EXCELIcon from './EXCEL-icon.vue'
import PDFIcon from './PDF-icon.vue'
import UNKNOWNIcon from './UNKNOWN-icon.vue'
import MOREIcon from './MORE-icon.vue'

export { PDFIcon, PPTIcon, WORDIcon, EXCELIcon, UNKNOWNIcon, MOREIcon }
