import IVImageView from './IVImageView/IVImageView.vue'
import IVModal from './IVModal/IVModal.vue'
import IVPagination from './IVPagination/IVPagination.vue'
import IVTable from './IVTable/IVTable.vue'
import IVTableControl from './IVTable/IVTableControl.vue'

export { IVImageView, IVModal, IVPagination, IVTable, IVTableControl }
