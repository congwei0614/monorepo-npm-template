/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-08-08 18:34:11
 * @LastEditTime: 2023-06-07 10:04:02
 */
import { defineConfig } from 'rollup'
import commonjs from '@rollup/plugin-commonjs'
import { nodeResolve as resolve } from '@rollup/plugin-node-resolve'
import { babel } from '@rollup/plugin-babel'
import json from '@rollup/plugin-json'
import typescript from 'rollup-plugin-typescript2'
import postcss from 'rollup-plugin-postcss'
import vue from 'rollup-plugin-vue'
import image from '@rollup/plugin-image'
import cleanup from 'rollup-plugin-cleanup' // 清楚无用逻辑
import { terser } from 'rollup-plugin-terser' // 压缩代码
import sourcemaps from 'rollup-plugin-sourcemaps' // 生成map映射
import copy from 'rollup-plugin-copy'
import del from 'rollup-plugin-delete'
import * as sass from 'sass'

import packageConfig from './package.json' assert { type: 'json' }

export default defineConfig([
  {
    input: 'src/index.ts',
    external: ['vue'],
    plugins: [
      del({ targets: './dist/*' }),
      vue(),
      typescript({ check: false }),
      resolve({
        preferBuiltins: true,
        browser: true,
        extensions: ['.ts', '.js', '.vue', '.sass', 'scss']
      }),
      json(),
      commonjs(),
      postcss({
        extract: true,
        minimize: true,
        extensions: ['.css', '.scss'],
        use: ['sass'],
        process: sass
      }),

      babel({
        presets: ['@babel/preset-env'],
        plugins: [
          '@babel/plugin-proposal-object-rest-spread',
          [
            '@babel/plugin-transform-runtime',
            {
              absoluteRuntime: false,
              helpers: false,
              regenerator: true,
              useESModules: true
            }
          ]
        ],
        babelrc: false,
        extensions: ['.js', '.ts', '.tsx', '.jsx', '.es6', '.es', '.mjs', '.vue']
      }),

      image(),
      copy({
        targets: [{ src: 'src/Altimeter', dest: 'dist' }]
      }),

      sourcemaps(),
      terser(),
      cleanup()
    ],
    output: [
      {
        sourcemap: true,
        name: packageConfig.name,
        file: './dist/index.esm.js',
        format: 'es'
      }
      // {
      //   sourcemap: true,
      //   name: packageConfig.name,
      //   file: './dist/index.cjs.js',
      //   format: 'commonjs'
      // }
      // {
      //   sourcemap: true,
      //   name: packageConfig.name,
      //   file: './dist/index.umd.js',
      //   format: 'umd',
      //   globals: {
      //     window: 'window' // 指定全局变量 window 对应的 UMD 外部依赖
      //   }
      // }
    ]
  }
])
