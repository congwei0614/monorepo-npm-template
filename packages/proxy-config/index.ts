import {
  HOST_KEY_ENUM,
  HOST_KEY_ENUM_KEY,
  HOST_KEY_ENUM_VALUE,
  MODULE_ENUM,
  MODULE_ENUM_KEY,
  MODULE_ENUM_VALUE,
  ModuleConfig,
  ProxyConfig,
  ProxyOption
} from './types'

export type {
  HOST_KEY_ENUM,
  HOST_KEY_ENUM_KEY,
  HOST_KEY_ENUM_VALUE,
  MODULE_ENUM,
  MODULE_ENUM_KEY,
  MODULE_ENUM_VALUE,
  ModuleConfig,
  ProxyConfig,
  ProxyOption
}

// 端口号
// 地址
const TIME_OUT = 1000 * 60 * 10
let BASE_PORT = 3000

const moduleConfig: ModuleConfig = Object.keys(MODULE_ENUM).reduce((acc: ModuleConfig, cur) => {
  const key = cur as MODULE_ENUM_KEY
  acc[key] = { key: MODULE_ENUM[key], BASE_PORT: BASE_PORT++, TIME_OUT }
  return acc
}, {} as ModuleConfig)

// 获取模块信息
export const getModuleConfig = (name: MODULE_ENUM_KEY) => moduleConfig[name]

const proxyConfig: ProxyConfig = Object.keys(HOST_KEY_ENUM).reduce((acc: ProxyConfig, cur) => {
  const key = cur as HOST_KEY_ENUM_KEY
  acc[key] = {
    key: HOST_KEY_ENUM[key],
    dev: `https://${HOST_KEY_ENUM[key]}.nacho.cn/`,
    prod: `https://${HOST_KEY_ENUM[key]}.stecip.com/`
  }
  return acc
}, {} as ProxyConfig)

// 获取代理信息
export const getProxyConfig = (name: HOST_KEY_ENUM_KEY) => proxyConfig[name]
// 路由代理配置信息
export const proxyOption: ProxyOption = Object.keys(HOST_KEY_ENUM).reduce((acc: ProxyOption, cur) => {
  const { dev, key } = getProxyConfig(cur as HOST_KEY_ENUM_KEY)
  acc[`/${key}`] = {
    target: dev, // 后端服务实际地址
    changeOrigin: true, //开启代理
    secure: false, // https使用 禁用ssl证书
    rewrite: (path: string) => path.replace(`/${key}`, '')
  }
  return acc
}, {} as ProxyOption)
