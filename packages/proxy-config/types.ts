export enum MODULE_ENUM {
  'MRP2' = 'mrp2',
  'BIA' = 'bia',
  'BMS' = 'bms',
  'WORKCENTER' = 'workcenter',
  'DEAL' = 'deal',
  'PES' = 'pes',
  'SAFE' = 'safe',
  'APS' = 'aps',
  'WDM' = 'wdm',
  'POS' = 'pos',
  'SC' = 'sc',
  'MRP' = 'mrp',
  'ASSETS' = 'assets',
  'RMS' = 'rms',
  'SF' = 'sf',
  'QRM' = 'qrm',
  'FMM' = 'fmm',
  'PDM' = 'pdm',
  'BASIS' = 'basis',
  'FMS' = 'fms',
  'PLM' = 'plm',
  'CCFLOW' = 'ccflow'
}

export type MODULE_ENUM_KEY = keyof typeof MODULE_ENUM
export type MODULE_ENUM_VALUE = (typeof MODULE_ENUM)[keyof typeof MODULE_ENUM]

export type ModuleConfig = {
  [key in MODULE_ENUM_KEY]: { key: string; TIME_OUT: number; BASE_PORT: number }
}

export enum HOST_KEY_ENUM {
  'ASSET' = 'asset',
  'GATEWAY' = 'gateway',
  'BASIS' = 'basis',
  'MRP' = 'mrp',
  'MRP2' = 'mrp2',
  'FMM' = 'fmm',
  'MC' = 'mc',
  'RMS' = 'rms',
  'WORKCENTER' = 'workcenter',
  'PDM' = 'pdm',
  'APS' = 'aps',
  'PES' = 'pes',
  'RDC' = 'rdc',
  'QRM' = 'qrm',
  'WDM' = 'wdm',
  'ODS' = 'ods',
  'SAFE' = 'safe',
  'POS' = 'pos',
  'PLM' = 'plm',
  'SF' = 'sf'
}
export type HOST_KEY_ENUM_KEY = keyof typeof HOST_KEY_ENUM
export type HOST_KEY_ENUM_VALUE = (typeof HOST_KEY_ENUM)[keyof typeof HOST_KEY_ENUM]
export type ProxyConfig = {
  [key in HOST_KEY_ENUM_KEY]: { key: string; dev: string; prod: string }
}

export type ProxyOption = Record<
  string,
  { target: string; changeOrigin: boolean; secure: boolean; rewrite: (path: string) => string }
>
