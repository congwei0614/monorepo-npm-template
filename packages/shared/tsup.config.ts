/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-08-08 18:23:38
 * @LastEditTime: 2022-08-09 17:28:57
 */
import { defineConfig } from 'tsup'

export default defineConfig({
  entry: ['./src/index.ts'],
  clean: true,
  dts: true,
  minify: true,
  outDir: 'dist',
  format: ['esm', 'cjs']
})
