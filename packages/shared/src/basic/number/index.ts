import { divide, minus, plus, round, strip, times } from 'number-precision'

/**
 * @description 类型守卫：判断某个key是否存在对象中
 */
const isKeyInObject = (object: object, key: string): key is keyof typeof object => {
  return Object.prototype.hasOwnProperty.call(object, key)
}

/**
 * @description 计算两个数字的百分百比
 * @param {Number} complete 分子
 * @param {Number} need 分母
 * @return {Number} complete / need
 */
export const formatPercentage = (complete: number, need: number) => {
  if (!need || !complete) return 0
  const result = times(divide(complete / need), 100)
  return Number.isNaN(result) ? 0 : result
}

/**
 * @description 计算数组之和
 * @param {Number[]} number[]
 * @return {Number}
 */
export const arraySum = (arr: number[]) => {
  const len = arr.length
  if (len === 0) return 0
  if (len === 1) return arr[0]
  return plus(...arr)
}

/**
 * @description 计算数组某个字段之和
 * @param {object[]} object[]
 * @return {Number}
 */
export const arraySumByKey = (arr: any[], key: string) => {
  const len = arr.length
  if (len === 0) return 0
  if (len === 1) {
    if (!isKeyInObject(arr[0], key)) throw new Error(`could not find it '${key}' in arraySumByKey function`)
    return Number(arr[0][key]) ?? 0
  }
  const tempArr: number[] = []
  arr.forEach(_e => {
    if (!isKeyInObject(_e, key)) throw new Error(`could not find it '${key}' in arraySumByKey function`)
    tempArr.push(_e[key] ?? 0)
  })
  return arraySum(tempArr)
}

/**
 * @description 对比两个数组某个字段之和是否相同
 * @param {object[]} object1[]
 * @param {object[]} object2[]
 * @return {Number}
 */

export const isArraySumCompared = (arr1: any[], arr2: any[], key1: string, key2?: string) => {
  const k1 = key1
  const k2 = key2 ?? key1
  return arraySumByKey(arr1, k1) == arraySumByKey(arr2, k2)
}

/**
 * @description 格式化数字，增加千分位标记和保留小数
 * @param {Number} num
 * @param {Number} suffix 默认保留两位小数
 * @return {String}
 */
export const numFormat = (num: number, suffix = 2) => {
  const value = Number(num)
  if (Number.isNaN(value)) return '0.00'
  return value.toLocaleString('en-US', {
    minimumFractionDigits: suffix,
    maximumFractionDigits: suffix
  })
}

export { strip, plus, minus, times, divide, round }
