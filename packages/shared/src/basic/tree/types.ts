// 数组转树结构（多级）
export interface TreeList {
  parentId: string | null
  id: string
  children?: TreeList[]
}
