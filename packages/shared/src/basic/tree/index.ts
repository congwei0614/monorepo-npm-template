import type { TreeList } from './types'
// 数组转树结构（两级）
export function arrToTree(list: any[], parentMark: string, childrenMark: string) {
  // parentMark 父亲的标记
  // childrenMark 孩子的标记
  const result: any = []
  let temp = null
  for (let i = 0; i < list.length; i++) {
    if (!list[i][parentMark]) {
      temp = list.filter((item: any) => item[parentMark] === list[i][childrenMark])
      if (temp.length > 0) {
        list[i].children = temp
      }
      result.push(list[i])
    }
  }
  return result
}

//加载子项，从list里加载obj的子项
function _loadChildren<T extends TreeList>(obj: T, list: T[]) {
  const arr = list.filter(x => x.parentId == obj.id)
  if (!arr.length) return
  //加载下一层子项
  obj.children = arr
  //子项也要遍历，递归查找子项的下一级子项
  obj.children.forEach(x => {
    _loadChildren(x, list)
  })
  //返回结果
  return obj
}

/**
 * 根据parentId===id进行判断
 */
export function tranListToTreeData<T extends TreeList>(list: T[]) {
  //找出结果，parentId为空的就是第一层
  const results = list.filter(x => x.parentId == null || !list.some(xx => x.parentId === xx.id))

  //每个去加载自己的子项
  results.forEach(x => _loadChildren<TreeList>(x, list))
  return results
}

/**
 * 树结构查询元素
 */
export function findTree<T extends { id: string; children?: T[] }>(id: string, list: T[], idName?: any) {
  let result
  if (!list.length) return
  const fn = (id: string, list: T[], idName?: any) => {
    list.forEach((_e: any) => {
      if (_e[idName] === id) {
        result = _e
      } else if (_e.children && _e.children.length !== 0) return fn(id, _e.children, idName)
    })
  }
  fn(id, list, idName)
  return result
}
