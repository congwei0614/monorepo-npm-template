import useGetTableList from './useGetTableList'
import usePermission from './usePermission'

export { useGetTableList, usePermission }
