/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-11-07 09:57:58
 * @LastEditTime: 2023-11-03 10:43:48
 */

import { SYSTEM_ENUM, TYPE_ENUM, getFuncInfo } from './PermissionEnum'

// * 大小写转换
// * 边界问题

/**
 * 权限判断函数
 *
 * @params value 具体功能名
 * @params funcKey 某一个功能模块enum值，具体值查看枚举文件（relationMap对象children）
 * @params type 操作功能enum值，具体值查看枚举文件（TYPE_ENUM）,如果权限没有在枚举文件列举，请直接写入权限名称
 */

export default function hasPermission(value: string, funcKey: number, type: number | string): boolean {
  const permissionList = localStorage && JSON.parse(localStorage.getItem('AccessData') ?? '{}')
  const selectModule = localStorage && JSON.parse(localStorage.getItem('selectModule') ?? '{}')

  const config: {
    SYSTEM: keyof typeof SYSTEM_ENUM
    MANAGEMENT: any
  } = {
    // SYSTEM: "BussinessCenter",
    // MANAGEMENT: "YieldModule",
    SYSTEM: selectModule.key.split('.')[0], // 系统配置
    MANAGEMENT: selectModule.key.split('.')[1] // 权限配置
  }

  function getBasePermission() {
    // 获取当前系统权限
    const centerList = permissionList.find((_e: any) => _e.name === config.SYSTEM)
    if (!centerList?.isGranted) return { moduleList: null }
    // 系统级别权限列表
    const managementList = centerList?.permissions
    const moduleList = managementList.filter((_e: any) => _e.name.includes(config.MANAGEMENT))
    return { moduleList }
  }

  // 获取当前子系统全部权限列表
  const { moduleList } = getBasePermission()

  // 根据funcKey获取当前子系统某一模块名称
  const { funcInfo } = getFuncInfo(config, funcKey)
  // console.log("funcInfo", funcInfo);
  // 获取某一模块全部权限
  const funcList = moduleList.filter((_e: any) => _e.name.includes(funcInfo))
  // console.log("---------------funcList", funcList);
  // 获取模块某一功能全部权限
  const itemList = funcList.filter((_e: any) => _e.name.includes(value))
  // console.log("---------------itemList", itemList);
  if (type === 0) {
    // 列表处理
    const result = itemList.find((_e: any) => _e.name.endsWith(value))
    return result?.isGranted
  } else {
    const result = itemList.find(
      (_e: any) => _e.name.split('.').at(-1) === (typeof type === 'number' ? TYPE_ENUM[type] : type)
    )
    return result?.isGranted
  }
}
