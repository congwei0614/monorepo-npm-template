/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-11-08 09:55:52
 * @LastEditTime: 2023-11-02 10:57:30
 */
export enum SYSTEM_ENUM {
  'EconomyCenter' = 0, // 经营中心
  'BussinessCenter' = 1, // 订单管理
  'TechCenter' = 2, // 技术中心
  'MaterialCenter' = 3, // 物资中心
  'AdministrationCenter' = 4, // 行政中心
  'BasisSystem' = 5 // 通用系统
}

//---------------经营中心---------------------------------------------------------
enum ECONOMYCENTER_ENUM {
  'OrderManagement' = 0, // 订单管理
  'BudgetManagement' = 1, // 经营分析
  'BillManagement' = 2, // 票据管理
  'CapitalManagement' = 3, // 资金管理
  'CenterManagement' = 4, // 中心管理
  'QuotaManagement' = 5 // 定额管理
}
enum ECONOMYCENTER_ORDERMANAGEMENT_ENUM {
  'EngineeringManagement' = 0, // 工程管理
  'InternalSubcontracting' = 1, // 内部分包管理
  'ExternalSubcontracting' = 2, // 外部分包管理
  'BussinessCenter' = 3, // 中心管理
  'BaseConfiguration' = 4, // 基础配置
  'BussinessPersonnel' = 5 // 使用和权限配置
}
enum ECONOMYCENTER_BUDGETMANAGEMENT_ENUM {
  'DimensionAnalysis' = 0, // 维度分析
  'ProjectAnalysis' = 1, // 项目分析
  'Center' = 2, // 中心
  'ManageUnit' = 3 // 管理主体
}
enum ECONOMYCENTER_BILLMANAGEMENT_ENUM {
  'BasicConfiguration' = 0, // 公共基础配置
  'BillInfo' = 1, // 票据管理
  'BillPersonnel' = 2 // 使用和权限配置
}
enum ECONOMYCENTER_CAPITALMANAGEMENT_ENUM {}
enum ECONOMYCENTER_CENTERMANAGEMENT_ENUM {}
enum ECONOMYCENTER_QUOTAMANAGEMENT_ENUM {}

//---------------订单管理---------------------------------------------------------
enum BUSSINESSCENTER_ENUM {
  'ProductModule' = 0, // 生产管理
  'SecurityModule' = 1, // 安全管理
  'LogisticsModule' = 2, // 物流管理
  'InventoryModule' = 3, // 库存管理
  'YieldModule' = 4 // 产量管理
}
enum BUSSINESSCENTER_PRODUCTMODULE_ENUM {}
enum BUSSINESSCENTER_SECURITYMODULE_ENUM {
  'HiddenDanger' = 0 // 隐患管理
}
enum BUSSINESSCENTER_LOGISTICSMODULE_ENUM {}
enum BUSSINESSCENTER_INVENTORYMODULE_ENUM {}
enum BUSSINESSCENTER_YIELDMODULE_ENUM {
  'ProjectListMenu' = 0, // 项目管理
  'ProductListMenu' = 1, // 产品管理
  'BussinessManegeMenu' = 2 //运营管理
}

//---------------技术中心---------------------------------------------------------
enum TECHCENTER_ENUM {
  'TechConfiguratio' = 0, // 技术配置
  'ExperimentManagement' = 1, // 试验管理
  'ScientificManagement' = 2 // 科研管理
}
enum TECHCENTER_TECHCONFIGURATIO_ENUM {}
enum TECHCENTER_EXPERIMENTMANAGEMENT_ENUM {}
enum TECHCENTER_SCIENTIFICMANAGEMENT_ENUM {}

//---------------物资中心---------------------------------------------------------
enum MATERIALCENTER_ENUM {
  'MaterialModule' = 0, // 材料管理
  'AssetsModule' = 1, // 资产管理
  'RentSaleModule' = 2, // 租售管理
  'FacilityMonitoringModule' = 3 // 设备监控
}
enum MATERIALCENTER_MATERIALMODULE_ENUM {
  'Purchase' = 0, // 采购
  'Inventory' = 1, // 实物入库移库
  'Acceptance' = 2, // 验收
  'Pay' = 3, // 付款
  'Store' = 4, // 收发存情况
  'Material' = 6, // 材料
  'Fundcost' = 7, // 报表
  'BasicConfiguration' = 8 // 基础配置
}
enum MATERIALCENTER_ASSETSMODULE_ENUM {
  'Contract' = 0, // 合同管理
  'WorkMetering' = 1, // 工作计量
  'CostPayment' = 2, // 费用付款
  'CollectionSituation' = 3, // 收款情况
  'Base' = 4 // 基础配置
}
enum MATERIALCENTER_RENTSALEMODULE_ENUM {}
enum MATERIALCENTER_FACILITYMONITORINGMODULE_ENUM {}

//---------------行政中心---------------------------------------------------------
enum ADMINISTRATIONCENTER_ENUM {
  'PartyModule' = 0, // 党建管理
  'PersonnelModule' = 1, // 人事管理
  'GeneralModule' = 2, // 总经管理
  'PlanModule' = 3 // 企划管理
}
enum ADMINISTRATIONCENTER_PARTYMODULE_ENUM {}
enum ADMINISTRATIONCENTER_PERSONNELMODULE_ENUM {}
enum ADMINISTRATIONCENTER_GENERALMODULE_ENUM {}
enum ADMINISTRATIONCENTER_PLANMODULE_ENUM {}

//---------------通用系统---------------------------------------------------------
enum BASISSYSTEM_ENUM {
  'PermissionModule' = 0, // 基础配置
  'OtherFeeModule' = 1, // 费用管理
  'OrganizationModule' = 2, // 组织管理
  'ProductLineModule' = 3, // 产线管理
  'ProcessModule' = 4 // 工序管理
}
enum BASISSYSTEM_PERMISSIONMODULE_ENUM {
  'BasisPersonnel' = 0, //使用者管理
  'ExternalOrg' = 1, //外部组织管理
  'BasicConfiguration' = 2, //公共基础配置
  'EconomicBasic' = 3, //经济系统基础配
  'MesBasic' = 4 //生产系统基础配置
}
enum BASISSYSTEM_OTHERFEEMODULE_ENUM {
  'Contracts' = 0, //合同
  'Costs' = 1, //
  'Pay' = 2, //付款
  'Base' = 3 //基础配置
}
enum BASISSYSTEM_ORGANIZATIONMODULE_ENUM {}
enum BASISSYSTEM_PRODUCTLINEMODULE_ENUM {}
enum BASISSYSTEM_PROCESSMODULE_ENUM {}

export enum TYPE_ENUM {
  'List' = 0, //列表
  'Create' = 1, //新增
  'Update' = 2, //编辑
  'Delete' = 3, //删除
  'Detail' = 4, //详情
  'Export' = 5, //导出
  'Print' = 6 // 打印
}

const relationMap: any = {
  0: {
    id: ECONOMYCENTER_ENUM,
    children: {
      0: ECONOMYCENTER_ORDERMANAGEMENT_ENUM,
      1: ECONOMYCENTER_BUDGETMANAGEMENT_ENUM,
      2: ECONOMYCENTER_BILLMANAGEMENT_ENUM,
      3: ECONOMYCENTER_CAPITALMANAGEMENT_ENUM,
      4: ECONOMYCENTER_CENTERMANAGEMENT_ENUM,
      5: ECONOMYCENTER_QUOTAMANAGEMENT_ENUM
    }
  },
  1: {
    id: BUSSINESSCENTER_ENUM,
    children: {
      0: BUSSINESSCENTER_PRODUCTMODULE_ENUM,
      1: BUSSINESSCENTER_SECURITYMODULE_ENUM,
      2: BUSSINESSCENTER_LOGISTICSMODULE_ENUM,
      3: BUSSINESSCENTER_INVENTORYMODULE_ENUM,
      4: BUSSINESSCENTER_YIELDMODULE_ENUM
    }
  },
  2: {
    id: TECHCENTER_ENUM,
    children: {
      0: TECHCENTER_TECHCONFIGURATIO_ENUM,
      1: TECHCENTER_EXPERIMENTMANAGEMENT_ENUM,
      2: TECHCENTER_SCIENTIFICMANAGEMENT_ENUM
    }
  },
  3: {
    id: MATERIALCENTER_ENUM,
    children: {
      0: MATERIALCENTER_MATERIALMODULE_ENUM,
      1: MATERIALCENTER_ASSETSMODULE_ENUM,
      2: MATERIALCENTER_RENTSALEMODULE_ENUM,
      3: MATERIALCENTER_FACILITYMONITORINGMODULE_ENUM
    }
  },
  4: {
    id: ADMINISTRATIONCENTER_ENUM,
    children: {
      0: ADMINISTRATIONCENTER_PARTYMODULE_ENUM,
      1: ADMINISTRATIONCENTER_PERSONNELMODULE_ENUM,
      2: ADMINISTRATIONCENTER_GENERALMODULE_ENUM,
      3: ADMINISTRATIONCENTER_PLANMODULE_ENUM
    }
  },
  5: {
    id: BASISSYSTEM_ENUM,
    children: {
      0: BASISSYSTEM_PERMISSIONMODULE_ENUM,
      1: BASISSYSTEM_OTHERFEEMODULE_ENUM,
      2: BASISSYSTEM_ORGANIZATIONMODULE_ENUM,
      3: BASISSYSTEM_PRODUCTLINEMODULE_ENUM,
      4: BASISSYSTEM_PROCESSMODULE_ENUM
    }
  }
}

export function getFuncInfo(
  config: {
    SYSTEM: keyof typeof SYSTEM_ENUM
    MANAGEMENT: any
  },
  i: number
) {
  const systemIndex = SYSTEM_ENUM[config.SYSTEM]
  const systemMap = relationMap[SYSTEM_ENUM[config.SYSTEM]]

  const managementIndex = systemMap.id[config.MANAGEMENT]
  const managementIndexMap = systemMap.children[managementIndex]

  const funcInfo = managementIndexMap[i]
  return {
    systemIndex,
    systemMap,
    managementIndex,
    managementIndexMap,
    funcInfo
  }
}
