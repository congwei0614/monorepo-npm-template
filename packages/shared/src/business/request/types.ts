import { HOST_KEY_ENUM_KEY } from '@iv/proxy-config'
import type { AxiosRequestConfig, AxiosResponse, InternalAxiosRequestConfig } from 'axios'
/**
 * 实例拦截器类型
 */
export interface RequestInterceptors {
  // 请求拦截
  requestInterceptors?: (config: InternalAxiosRequestConfig) => InternalAxiosRequestConfig
  requestInterceptorsCatch?: (err: any) => any
  // 响应拦截
  responseInterceptors?: <T = AxiosResponse>(config: T) => T
  responseInterceptorsCatch?: (err: any) => any
}
// 自定义传入的参数

/**
 * 因为axios提供的AxiosRequestConfig是不允许我们传入拦截器的，
 * 所以说我们自定义了RequestConfig，让其继承与AxiosRequestConfig。
 */
export interface RequestConfig extends AxiosRequestConfig {
  interceptors?: RequestInterceptors
  intactData?: boolean
}

export interface MrResponse extends AxiosResponse {
  intactData?: boolean
}

export interface CancelRequestSource {
  [index: string]: () => void
}

type KeyWithSuffix<Suffix extends string> = `${HOST_KEY_ENUM_KEY}${Suffix}`

export interface MyRequestConfig<T> extends RequestConfig {
  url: string
  data?: T
  requestBase?: KeyWithSuffix<'_URL'>
  intactData?: boolean
  [key: string]: any
}
export interface MyResponse<T> {
  statusCode: number
  desc: string
  result: T
}
