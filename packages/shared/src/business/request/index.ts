import qs from 'qs'
import { apiConfiger } from '../index'
import Request from './base'
import type { MyRequestConfig } from './types'

const TIME_OUT = 1000 * 60 * 10

const headers: any = {
  'Content-Type': 'application/json;charset=UTF-8',
  'Accept-Language': 'zh-Hans'
}

const request = new Request({
  timeout: TIME_OUT,
  headers,
  interceptors: {
    // 请求拦截器
    requestInterceptors: (config: any) => {
      const token = localStorage.getItem('token')
      config.headers.authorization = token ? `Bearer ${token}` : null
      return apiConfiger(config)
    },
    // 响应拦截器
    responseInterceptors: result => {
      return result
    }
  }
})

/**
 * @description: 函数的描述
 * @interface D 请求参数的interface
 * @interface T 响应结构的intercept
 * @param {MyRequestConfig} config 不管是GET还是POST请求都使用data
 * @returns {Promise}
 */
const cwRequest = <D, T = any>(config: MyRequestConfig<D>) => {
  const { method = 'GET' } = config
  if (method === 'get' || method === 'GET') {
    config.paramsSerializer = (params: any) => {
      return qs.stringify(params, { arrayFormat: 'repeat' })
    }
    for (const i in config.data) {
      if (config.data[i] === null || config.data[i] === undefined) {
        delete config.data[i]
      }
    }
    config.params = config.data
  }
  return request.request<T>(config)
}
// 取消请求
export const cancelRequest = (url: string | string[]) => {
  return request.cancelRequest(url)
}
// 取消全部请求
export const cancelAllRequest = () => {
  return request.cancelAllRequest()
}

export default cwRequest
