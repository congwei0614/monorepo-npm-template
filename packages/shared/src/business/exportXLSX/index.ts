export default (_v: Blob, _o: { header: any }, _d?: { fileName: string }) => {
  const d = {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8',
    'content-disposition': 'attachment; filename=%E6%8A%A5%E8%A1%A8%E5%AF%BC%E5%87%BA.xlsx'
  }
  const option = {
    type: _o.header['content-type'] ?? d.type,
    // type: "application/vnd.ms-excel",
    'content-disposition': _o.header['content-disposition'] ?? d['content-disposition']
  }
  /**
   * var aBlob = new Blob( array, options );
   * array 是一个由ArrayBuffer, ArrayBufferView, Blob, DOMString 等对象构成的 Array
   * 所以传递过来的_v参数一定要符合要求，也就是说返回的数据要已经是blob类型。
   * 通过axios的responseType来修改响应数据，将字节流就改为blob
   * */
  const blob = new Blob([_v], {
    type: option.type
  })
  const downloadElement = document.createElement('a')
  const href = window.URL.createObjectURL(blob) //创建下载的链接
  downloadElement.href = href
  downloadElement.download = _d?.fileName || decodeURI(option['content-disposition'].split('filename=')[1]) || '' //下载后文件名
  document.body.appendChild(downloadElement)
  downloadElement.click() //点击下载
  document.body.removeChild(downloadElement) //下载完成移除元素
  window.URL.revokeObjectURL(href) //释放掉blob对象
}
