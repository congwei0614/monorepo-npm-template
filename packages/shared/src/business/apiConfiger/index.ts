/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-08-19 17:45:37
 * @LastEditTime: 2023-10-26 14:08:48
 */
import { getProxyConfig } from '@iv/proxy-config'
export default function apiConfiger(config: any) {
  const hostname = window.location.hostname.split('.')
  const { dev, prod, key } = getProxyConfig(config.requestBase.replace(/_URL/, ''))
  if (hostname.includes('localhost')) {
    config.baseURL = `/${key}`
  } else if (hostname.includes('nacho')) {
    config.baseURL = dev
  } else if (hostname.includes('stecip')) {
    config.baseURL = prod
  }
  return config
}
