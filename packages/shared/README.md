# @IV/shared

## 快速开始

> npm i iv-npm

样式引入

```ts
// main.ts
// 此处不包含antv样式，需要本地引入，尽量保证样式在最后
import '@iv/ui/dist/index.esm.css'
```



全局注册引入

```ts
// main.ts
import ivUi from "iv-npm/packages/ui";
app.use(ivUi);
```

按需引入

```vue
// UI组件使用方法
<template>
  <IVModal v-model:show="Visible"></IVModal>
</template>
<script setup lang="ts">
import { IVModal } from "iv-npm";
</script>
```

**推荐自动按需引入**

```js
// vite.config.ts
{
     Components({
      resolvers: [
        importName => {
          if (importName.startsWith('IV')) return { importName, path: `@iv/ui` }
        },
          ...otherResolvers
      ]
    })
}
// ts.config.ts
{
  ...otherConfig,
  "include": [...,"components.d.ts"],
}
```



## 组织架构/部门下拉

> <IVOrgDropdown />

- 组件 API

  ```js
  {
    method: "GET",
    url:'/api/identity/users/allOrganizationByUser',
    params:{type:1},
  }
  ```

  

- 示例

```vue
<template>
  <IVOrgDropdown v-model:value="Detail.organizationId" :params="{type:1}"
  :data="{name:123}" :replaceFields="{ children: "children", label:
  "displayName", key: "id", value: "id", }" />
</template>
<script setup lang="ts">
import { IVOrgDropdown } from "iv-npm/packages/ui";
</script>
```

| props         | describe                                | type                  |
| ------------- | --------------------------------------- | --------------------- |
| v-model:value | 绑定值                                  | any                   |
| params        | GET 请求参数(响应式，值改变会触发接口)  | object                |
| data          | POST 请求参数(响应式，值改变会触发接口) | object                |
| fieldNames    | 指定 value，label 字段名                | FieldNames,存在默认值 |
| inputStyle    | input 样式                              | object                |
| formItemStyle | 表单 formItem 的样式                    | object                |
| labelCol      | 表单 formItem 的 labelCol               | object                |
| wrapperCol    | 表单 formItem 的 wrapperCol             | object                |
| isRequired    | 是否必填                                | boolean               |
| multiple      | 是否多选                                | boolean               |
| isTitle       | 是否显示标题                            | boolean               |
| placeholder   | 预览文字                                | string                |
| title         | 标题                                    | string                |

## 生产基地下拉

> <IVProdBaseDropdown/>

- 组件 API

  ```js
  {
    method: "GET",
    url:'/api/basis-management/production-base/all',
  }
  ```

  

- 示例

```vue
<template>
  <IVProdBaseDropdown v-model:value="Detail.organizationIds" :params="{type:1}"
  :data="{name:123}" :fieldNames="{ label: "displayName", value: "id", }" />
</template>
<script setup lang="ts">
import { IVProdBaseDropdown } from "iv-npm/packages/ui";
</script>
```

| props             |
| ----------------- |
| 组织架构/部门下拉 |

## 条线下拉

> <IVLineDropdown/>

- 组件 API

  ```js
  {
    method: "GET",
    url:'/api/basis-management/production-base/all',
  }
  ```

  

- 示例

```vue
<template>
  <IVLineDropdown v-model:value="Detail.lineIds" :params="{type:1}"
  :data="{name:123}" :fieldNames="{ label: "displayName", value: "id", }" />
</template>
<script setup lang="ts">
import { IVLineDropdown } from "iv-npm/packages/ui";
</script>
```

| props             |
| ----------------- |
| 组织架构/部门下拉 |

## 账套下拉

> <IVSetofbookDropdown/>

- 组件 API

  ```js
  {
    method: "GET",
    url:'/api/basis-management/setOfBook/all',
  }
  ```

  

- 示例

```vue
<template>
  <IVSetofbookDropdown v-model:value="Detail.lineIds" :params="{type:1}"
  :data="{name:123}" :fieldNames="{ label: "displayName", value: "id", }" />
</template>
<script setup lang="ts">
import { IVSetofbookDropdown } from "iv-npm/packages/ui";
</script>
```

| props             |
| ----------------- |
| 组织架构/部门下拉 |
