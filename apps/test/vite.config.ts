/*
 * @Author: your name
 * @Date: 2022-03-29 13:04:32
 * @LastEditTime: 2023-06-13 13:30:16
 * @LastEditors: wanghao xh-pc@DESKTOP-CVB101Q
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \mrp2-web\vite.config.ts
 */
import { resolve } from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite'
import { AntDesignVueResolver, ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import { getModuleConfig, proxyOption } from '@iv/proxy-config'
import { IVResolver } from '@iv/shared'
import { ModuleName } from './src/setting/server.seeting'

const { BASE_PORT } = getModuleConfig(ModuleName)
function pathResolve(dir: string) {
  return resolve(process.cwd(), '.', dir)
}
// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: [
      {
        find: /\/@\//,
        replacement: `${pathResolve('src')}/`
      }
    ]
  },
  plugins: [
    vue(),
    Components({
      resolvers: [AntDesignVueResolver(), ElementPlusResolver(), IVResolver()]
    })
  ],
  server: {
    port: BASE_PORT,
    open: false, //自动打开
    host: '0.0.0.0',
    proxy: proxyOption
  },
  build: {
    chunkSizeWarningLimit: 2000,
    rollupOptions: {
      output: {
        manualChunks(id) {
          if (id.includes('node_modules')) {
            return id.toString().split('node_modules/')[1].split('/')[0].toString()
          }
        }
      }
    }
  }
})
