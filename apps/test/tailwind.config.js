/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-03-29 11:16:02
 * @LastEditTime: 2022-07-07 11:03:31
 */
module.exports = {
  content: ['./index.html', './src/**/*.{html,vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    fontFamily: {
      'ping-fang': ['PingFangSC-Regular', 'PingFang SC', 'sans-serif']
    }
  },
  plugins: []
}
