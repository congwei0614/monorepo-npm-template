// 定义 Pinia store
import { ref } from 'vue'
import { defineStore } from 'pinia'

export default defineStore('counter', () => {
  const keepAliveExclude = ref<string[]>([])

  const setKeepAliveExclude = (name: string) => {
    if (!keepAliveExclude.value.includes(name)) {
      keepAliveExclude.value.push(name)
    }
  }
  const removeKeepAliveExclude = (i: number) => {
    keepAliveExclude.value.splice(i, 1)
  }
  const removeKeepAliveExcludeByName = (name: string) => {
    const findIndex: any = keepAliveExclude.value.find(_x => _x === name)
    findIndex && keepAliveExclude.value.splice(findIndex, 1)
  }

  const pageArray = ref<string[]>([])
  const activePage = ref(0)
  const setPageArray = (i: number) => {
    pageArray.value.splice(i, 1)
  }
  const setActivePage = (id: number) => {
    activePage.value = id
  }
  const closeAllPage = () => {
    pageArray.value.splice(0, pageArray.value.length)
  }
  const closeCurrentPage = () => {
    pageArray.value.splice(-1, 1)
  }

  return {
    keepAliveExclude,
    setKeepAliveExclude,
    removeKeepAliveExclude,
    removeKeepAliveExcludeByName,
    pageArray,
    activePage,
    setPageArray,
    setActivePage,
    closeAllPage,
    closeCurrentPage
  }
})
