/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-07-07 11:10:21
 * @LastEditTime: 2023-12-28 09:39:40
 */

//  采购合同状态

export const projectStatusList = [
  {
    value: 0,
    label: '待开工'
  },
  {
    value: 1,
    label: '在建'
  },
  {
    value: 2,
    label: '完工未结算'
  },
  {
    value: 4,
    label: '完工已结算'
  },
  {
    value: 8,
    label: '已销项'
  }
]
export const partyATypeList = [
  {
    value: 1,
    label: '账套'
  },
  {
    value: 2,
    label: '客户'
  },
  {
    value: 3,
    label: '供应商'
  }
]
export const partyBTypeList = [
  {
    value: 1,
    label: '账套'
  },
  {
    value: 2,
    label: '客户'
  },
  {
    value: 3,
    label: '供应商'
  }
]
export const typeList = [
  {
    value: 1,
    label: '账套'
  },
  {
    value: 2,
    label: '客户'
  },
  {
    value: 3,
    label: '供应商'
  }
]
