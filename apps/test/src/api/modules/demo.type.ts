export type productionLineStations = {
  mouldId: string | null
  mouldName: string | null
  processCode: number
  processName: string
  segmentId: string | null
  segmentName: string | null
  segmentSerialNumber: string | null
  sort: number
  stationId: string | null
  stationName: string | null
  bgcName?: string
}
export type steamStations = {
  fire: boolean
  sort: number
  stationId: string | null
  stationName: string | null
  steamStatus: number
  temperature: number
  warning: boolean
  warningMessage: string | null
  moulds: moulds[]
}
export type moulds = {
  mouldId: string | null
  mouldName: string | null
  mouldSuiteId: string | null
  segmentId: string | null
  segmentName: string | null
  segmentSerialNumber: string | null
  sort: number
}
export type InCar = {
  mouldId: string
  mouldName: string
  segmentId: null
  segmentName: null
  segmentSerialNumber: null
}

export interface Req {
  q?: string
}
export interface Res {
  carPosition: number
  mouldDown: boolean
  mouldInCar: InCar
  mouldInProductionLineInput: InCar | null
  mouldInProductionLineOutput: InCar | null
  mouldUp: boolean
  productionLineId: string
  productionLineStations: productionLineStations[]
  smallCarInProductionLineInput: boolean
  smallCarInProductionLineOut: boolean
  smallCarReturn: boolean
  smallCarInSteam: boolean
  steamStations: steamStations[]
}
