/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-03-29 12:10:31
 * @LastEditTime: 2022-03-29 12:14:49
 */
import request from '../index'
import { Req, Res } from './demo.type'
export const getDashBoardLine3 = (data?: Req) => {
  return request<Req, Res>({
    url: '/api/manufacturingManagement/iots/info/DashBoard-Line3-Touch',
    method: 'GET',
    data
  })
}
