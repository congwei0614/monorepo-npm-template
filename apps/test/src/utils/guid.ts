const S4 = function () {
  return Math.trunc(1 + Math.random() * 0x10000)
    .toString(16)
    .slice(1)
}

export function guid() {
  return `${S4() + S4()}-${S4()}-${S4()}-${S4()}-${S4()}${S4()}${S4()}`
}
