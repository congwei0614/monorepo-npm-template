// 保留千分符、精确几位小数

export function numFormat(num: any, fixed: number = 0) {
  num = Number.isNaN(Number(num)) ? 0 : Number(num)
  if (fixed) num = num.toFixed(fixed)
  const res = num.toString().replace(/\d+/, (n: string) => {
    // 先提取整数部分
    return n.replace(/(\d)(?=(\d{3})+$)/g, ($1: string) => {
      return `${$1},`
    })
  })
  return res
}
