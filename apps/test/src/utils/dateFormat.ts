/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-05-11 09:49:56
 * @LastEditTime: 2023-07-03 11:54:31
 */
export type formatType = {
  date: string
  time: string
  week: string
}
/**
 * 函数重载，根据不同参数返回不同类型
 */
export function dateFormat(): formatType
export function dateFormat(data: Date | string, format: string): string
export function dateFormat(data?: Date | string, format?: string): string | formatType {
  type dataType = number | string
  const weekList = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
  const now = data ? new Date(data) : new Date()
  const year = now.getFullYear()
  let month: dataType = now.getMonth() + 1
  month = month < 10 ? `0${month}` : month
  let day: dataType = now.getDate()
  day = day < 10 ? `0${day}` : day
  let hours: dataType = now.getHours()
  hours = hours < 10 ? `0${hours}` : hours
  let minutes: dataType = now.getMinutes()
  minutes = minutes < 10 ? `0${minutes}` : minutes
  let seconds: dataType = now.getSeconds()
  seconds = seconds < 10 ? `0${seconds}` : seconds

  const week = weekList[now.getDay()]

  if (format) {
    format = format
      .replace(/YYYY/, String(year))
      .replace(/MM/, String(month))
      .replace(/DD/, String(day))
      .replace(/hh/, String(hours))
      .replace(/mm/, String(minutes))
      .replace(/ss/, String(seconds))
    return format
  } else {
    return {
      date: `${year}年${month}月${day}`,
      time: `${hours}:${minutes}:${seconds}`,
      week
    }
  }
}
export function yearMonthFormat(data?: Date): string | formatType {
  type dataType = number | string

  if (data) {
    const now = data ? new Date(data) : new Date()
    const year = now.getFullYear()
    const month: dataType = now.getMonth() + 1
    return `${year}年${month}月`
  } else {
    return ''
  }
}
export function yearMonthDayFormat(data?: Date): string | formatType {
  type dataType = number | string

  if (data) {
    const now = data ? new Date(data) : new Date()
    const year = now.getFullYear()
    const month: dataType = now.getMonth() + 1
    const day: dataType = now.getDate()
    return `${year}年${month}月${day}日`
  } else {
    return ''
  }
}
export function geToday() {
  const Now = new Date() // 当前中国标准时间
  const Year = Now.getFullYear() // 获取当前年份 支持IE和火狐浏览器
  const Month = Now.getMonth() + 1 // 获取中国区月份
  const Day = Now.getDate() // 获取几号
  let CurrentDate: string = Year.toString()
  if (Month >= 10) {
    // 判断月份和几号是否大于10或者小于10
    CurrentDate += Month
  } else {
    CurrentDate += `0${Month}`
  }
  if (Day >= 10) {
    CurrentDate += Day
  } else {
    CurrentDate += `0${Day}`
  }
  return CurrentDate
}
export function geSToday() {
  const Now = new Date() // 当前中国标准时间
  const Year = Now.getFullYear() // 获取当前年份 支持IE和火狐浏览器
  const Month = Now.getMonth() + 1 // 获取中国区月份
  const Day = Now.getDate() // 获取几号
  let CurrentDate: string = Year.toString().slice(2, 4)
  if (Month >= 10) {
    // 判断月份和几号是否大于10或者小于10
    CurrentDate += Month
  } else {
    CurrentDate += `0${Month}`
  }
  if (Day >= 10) {
    CurrentDate += Day
  } else {
    CurrentDate += `0${Day}`
  }
  return CurrentDate
}

// 保留千分符、精确几位小数
export function numFormat(num: any, fixed: number = 0) {
  num = Number.isNaN(Number(num)) ? 0 : Number(num)
  if (fixed) num = num.toFixed(fixed)
  const res = num.toString().replace(/\d+/, (n: string) => {
    // 先提取整数部分
    return n.replace(/(\d)(?=(\d{3})+$)/g, ($1: string) => {
      return `${$1},`
    })
  })
  return res
}
