/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-08-30 14:13:59
 * @LastEditTime: 2022-12-22 19:29:08
 */
import type { AxiosRequestConfig, AxiosResponse } from 'axios'
/**
 * 实例拦截器类型
 */
export interface RequestInterceptors {
  // 请求拦截
  requestInterceptors?: (config: AxiosRequestConfig) => AxiosRequestConfig
  requestInterceptorsCatch?: (err: any) => any
  // 响应拦截
  responseInterceptors?: <T = AxiosResponse>(config: T) => T
  responseInterceptorsCatch?: (err: any) => any
}
// 自定义传入的参数

/**
 * 因为axios提供的AxiosRequestConfig是不允许我们传入拦截器的，
 * 所以说我们自定义了RequestConfig，让其继承与AxiosRequestConfig。
 */
export interface RequestConfig extends AxiosRequestConfig {
  interceptors?: RequestInterceptors
  intactData?: boolean
}
export interface CancelRequestSource {
  [index: string]: () => void
}

export interface MyRequestConfig<T> extends RequestConfig {
  data?: T
  requestBase?: string
  intactData?: boolean
}
export interface MyResponse<T> {
  statusCode: number
  desc: string
  result: T
}
