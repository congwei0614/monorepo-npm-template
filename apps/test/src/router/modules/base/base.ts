/*
 * @Author: wanghao xh-pc@DESKTOP-CVB101Q
 * @Date: 2022-09-27 11:14:48
 * @LastEditors: wanghao xh-pc@DESKTOP-CVB101Q
 * @LastEditTime: 2023-12-22 16:28:26
 * @FilePath: \template-web\src\router\modules\base\base.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { RouteRecordRaw } from 'vue-router'
import layouts from '/@/layouts/layouts.vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: layouts,
    redirect: '/home',
    children: [
      {
        name: '主页',
        path: '/home',
        component: () => import('/@/views/home/index.vue')
      }
    ]
  },
  {
    path: '/auth',
    component: () => import('/@/views/login/auth.vue')
  },
  {
    path: '/login',
    component: () => import('/@/views/login/index.vue')
  }
]

export default routes
