/*
 * @Author: Mr.Cong Wei
 * @Date: 2022-03-29 10:56:00
 * @LastEditTime: 2022-08-19 09:42:27
 */
import { createApp } from 'vue'
import Antd from 'ant-design-vue'

import 'ant-design-vue/dist/antd.css'
import './index.css'
import * as Icons from '@ant-design/icons-vue'

// 开发模式组件包
import txUiDev from 'tiaoxiang-sider-dev'
import 'tiaoxiang-sider-dev/lib/style.css'

// 生产环境组件包
import txUi from 'tiaoxiang-sider'
import 'tiaoxiang-sider/lib/style.css'

import ivUi from 'iv-npm/packages/ui'
import { createPinia } from 'pinia'
import Router from './router/index'
import App from './App.vue'

import 'iv-npm/packages/ui/dist/index.umd.css'

import 'uc-npm/packages/core/dist/index.css'

import '@iv/ui/dist/index.esm.css'
// import iv from '@iv/ui'

const app = createApp(App)

if (process.env.NODE_ENV == 'development') {
  app.use(createPinia()).use(Router).use(Antd).use(ivUi).use(txUiDev).mount('#app')
} else {
  app.use(createPinia()).use(Router).use(Antd).use(ivUi).use(txUi).mount('#app')
}

// 循环使用全部全部图标
const icons: any = Icons
for (const i in icons) {
  // 全局注册一下组件
  app.component(i, icons[i])
}
