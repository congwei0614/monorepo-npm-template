import CImageView from './CImageView.vue'
import CModal from './CModal.vue'
import CPagination from './CPagination.vue'
import CTable from './CTable.vue'
import CTableControl from './CTableControl.vue'

export { CImageView, CModal, CPagination, CTable, CTableControl }
