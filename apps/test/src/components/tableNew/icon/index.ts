import PPTIcon from './imageView/PPT-icon.vue'
import WORDIcon from './imageView/WORD-icon.vue'
import EXCELIcon from './imageView/EXCEL-icon.vue'
import PDFIcon from './imageView/PDF-icon.vue'
import UNKNOWNIcon from './imageView/UNKNOWN-icon.vue'
import MOREIcon from './imageView/MORE-icon.vue'

import TableFooterIcon from './table/TableFooter-icon.vue'

import ModalHeaderIcon from './modal/ModalHeader-icon.vue'

export { PDFIcon, PPTIcon, WORDIcon, EXCELIcon, UNKNOWNIcon, MOREIcon, TableFooterIcon, ModalHeaderIcon }
