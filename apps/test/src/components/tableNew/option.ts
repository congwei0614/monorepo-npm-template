// 表单组件定义
export const defaultProps = {
  tableId: 'tableId',
  isFold: false,
  currentPages: 1,
  pageSize: 20,
  isClickItem: false,
  isMustSelected: false,
  isShowControl: true,
  size: 'default' as keyof typeof SIZEMAP
}

// 分页
export interface SpanParams {
  row: any
  rowIndex: number
  column: ThOption
  columnIndex: number
}
export type SpanHandle = (paramsSpan: SpanParams) => number

// 表头嵌套（eg.mrp2产品生产分配）
export interface HeaderChild {
  title: string
  w: string // 宽度
  unit?: string //单位
}

export interface ThOption {
  title: string // 标题
  key: string // 数据键 ,其中index为索引,pictures和pictureUrls为图片,buttonSlot、clickSlot、select为操作插槽
  w: string // 宽度
  show?: boolean // 是否显示列
  precision?: boolean // 是否精确数据（金额格式化,numFormat）
  dateFormAt?: string // 日期格式化模板串（eg.YYYY-MM-DD）
  custom?: boolean // 是否自定义内容（插槽,插槽名为 key,例如key:name,则插槽名为 name）
  customHeader?: boolean // 是否自定义表头（插槽,插槽名为 'th-'+key,例如key:name,则插槽名为 th-name）
  formAt?: (data: { v: any; o: any; th: ThOption }) => any // 数据格式化
  headerChild?: HeaderChild[] // 表头嵌套（eg
  unit?: string //单位
  colspanHandle?: SpanHandle // 合并列
  rowspanHandle?: SpanHandle // 合并行
}
export interface Option {
  data: any[]
  option: ThOption[]
  tableId?: string // 表格id
  isFold?: boolean // 是否开启折叠表格,通过控制data数据中的fold属性来实现展开和折叠。详情参考项目弹窗
  // 分页
  currentPages?: number //当前页码 // TODO ??
  pageSize?: number // 	每页条数 // TODO ??
  // 点击单元格相关属性
  isClickItem?: boolean // 是否开启点击表格行表格
  isMustSelected?: boolean // 是否必须存在一条选中数据
  clickId?: string // 点击行的id(v-model)
  // 控件
  isShowControl?: boolean
  size?: keyof typeof SIZEMAP
}

// 组件静态数据
export const SIZEMAP = {
  mini: { size: 20, title: '紧凑' },
  default: { size: 40, title: '默认' },
  large: { size: 60, title: '宽大' }
}
