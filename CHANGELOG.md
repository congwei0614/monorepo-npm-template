# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 2.2.0 (2024-05-06)


### Features

* **all:** :sparkles: init project ([de58a35](https://e.coding.net/itemvector/cloud-stec/tx-npm/commit/de58a35bb41dcb85109e9cd61bf33b80953acd39))
* **create-turbo:** apply official-starter transform ([5f2744b](https://e.coding.net/itemvector/cloud-stec/tx-npm/commit/5f2744bbc356fbe7b643ef62dd315efb595c0cc9))
* **create-turbo:** apply official-starter transform ([8869c02](https://e.coding.net/itemvector/cloud-stec/tx-npm/commit/8869c02459bd0120b3c11e400b6f61d6710ce27d))
* **create-turbo:** apply pnpm-eslint transform ([8621b2a](https://e.coding.net/itemvector/cloud-stec/tx-npm/commit/8621b2afb9dac85e04accdb4196d6443013ab3b3))
* **create-turbo:** apply pnpm-eslint transform ([afdd261](https://e.coding.net/itemvector/cloud-stec/tx-npm/commit/afdd261926e7390e0fd3dad5bd20dd38df29d26d))
* **create-turbo:** install dependencies ([164c271](https://e.coding.net/itemvector/cloud-stec/tx-npm/commit/164c27131ad0992f77b4b06aa48cd239331e38cd))


### Bug Fixes

* **all:** :bug: 文字修复 ([5f180b5](https://e.coding.net/itemvector/cloud-stec/tx-npm/commit/5f180b54c8afdd98f98ad81ca6938fa6995f19d6))
* **commitlint-config:** :bug: 修改git规范配置 ([2f2898d](https://e.coding.net/itemvector/cloud-stec/tx-npm/commit/2f2898d5e67a1d2cdda386f6c5be40e446df5971))
* **release:** :bug: 文字修复 ([875e79b](https://e.coding.net/itemvector/cloud-stec/tx-npm/commit/875e79b2a7a9d01e2f8d91dcca536fc290816be0))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
