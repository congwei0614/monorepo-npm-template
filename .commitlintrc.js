/*
 * @Author: Mr.Cong Wei
 * @Date: 2023-04-23 20:44:26
 * @LastEditTime: 2023-04-23 20:48:06
 */
/** @type {import('cz-git').UserConfig} */
module.exports = {
	rules: {
		// @see: https://commitlint.js.org/#/reference-rules
	},
	extends: ['@iv/commitizen-config'],
};